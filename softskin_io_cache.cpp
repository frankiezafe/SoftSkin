/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_io.cpp
 * Author: frankiezafe
 * 
 * Created on July 16, 2019, 3:02 PM
 */

#include "softskin_io_cache.h"

// SAVER

ResourceFormatSaverSoftskinCache::ResourceFormatSaverSoftskinCache() {
}

Error ResourceFormatSaverSoftskinCache::save(const String &p_path, const RES &p_resource, uint32_t p_flags) {
    const SoftskinCache* sc = Object::cast_to<SoftskinCache>(p_resource.ptr());
    if (!sc) {
        return FAILED;
    }
    SoftskinCache* sch = (SoftskinCache*) p_resource.ptr();
    return sch->save_file(p_path);
}

bool ResourceFormatSaverSoftskinCache::recognize(const RES & p_resource) const {
    return Object::cast_to<SoftskinCache>(p_resource.ptr()) != 0;
}

void ResourceFormatSaverSoftskinCache::get_recognized_extensions(const RES &p_resource, List<String> *p_extensions) const {
    const SoftskinCache* sc = Object::cast_to<SoftskinCache>(p_resource.ptr());
    if (sc) {
        p_extensions->clear();
        p_extensions->push_back(SSC_EXT);
    }

}

// LOADER

ResourceFormatLoaderSoftskinCache::ResourceFormatLoaderSoftskinCache() {
}

RES ResourceFormatLoaderSoftskinCache::load(const String &p_path, const String &p_original_path, Error * r_error) {
    SoftskinCache *sc = memnew(SoftskinCache);
    if (r_error) {
        *r_error = OK;
    }
    Error err = sc->load_file(p_path);
    if (err != OK) {
        return Ref<SoftskinCache>();
    }
    Ref<SoftskinCache> ss_cache = Ref<SoftskinCache>(sc);
    return ss_cache;
}

void ResourceFormatLoaderSoftskinCache::get_recognized_extensions(List<String> *p_extensions) const {
    p_extensions->push_back(SSC_EXT);
}

bool ResourceFormatLoaderSoftskinCache::handles_type(const String & p_type) const {
    return ClassDB::is_parent_class(p_type, SSC_NAME);
}

String ResourceFormatLoaderSoftskinCache::get_resource_type(const String & p_path) const {
    if (p_path.get_extension().to_lower() == SSC_EXT) {
        return SSC_NAME;
    }
    return "";
}