/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_io.h
 * Author: frankiezafe
 *
 * Created on July 16, 2019, 3:02 PM
 */

#ifndef SOFTSKIN_IO_CACHE_H
#define SOFTSKIN_IO_CACHE_H

#include "core/io/resource_saver.h"
#include "core/io/resource_loader.h"

#include "softskin_cache.h"

class ResourceFormatSaverSoftskinCache : public ResourceFormatSaver {
public:
    
        virtual Error save(const String &p_path, const RES &p_resource, uint32_t p_flags = 0);
        virtual bool recognize(const RES &p_resource) const;
        virtual void get_recognized_extensions(const RES &p_resource, List<String> *p_extensions) const;
        
        ResourceFormatSaverSoftskinCache();
        virtual ~ResourceFormatSaverSoftskinCache() {}
};

class ResourceFormatLoaderSoftskinCache : public ResourceFormatLoader {
public:
    
        virtual RES load(const String &p_path, const String &p_original_path, Error *r_error = NULL);
        virtual void get_recognized_extensions(List<String> *p_extensions) const;
        virtual bool handles_type(const String &p_type) const;
        virtual String get_resource_type(const String &p_path) const;
        
        ResourceFormatLoaderSoftskinCache();
        virtual ~ResourceFormatLoaderSoftskinCache() {}
};

#endif /* SOFTSKIN_IO_CACHE_H */

