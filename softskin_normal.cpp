/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_normal.cpp
 * Author: frankiezafe
 * 
 * Created on May 31, 2019, 4:45 PM
 */

#include "softskin_normal.h"

SoftskinNormal::SoftskinNormal() :
_ready(false) {
}

SoftskinNormal::~SoftskinNormal() {
}

void SoftskinNormal::push_back(SoftskinDot* d, SoftskinEdge* e0, SoftskinEdge* e1, bool flip0, bool flip1) {
    
    if (d != 0 && e0 != 0 && e1 != 0) {
        fdata fd;
        fd.dot = d;
        fd.edge0 = e0;
        fd.edge1 = e1;
        fd.flip0 = flip0 ? -1 : 1;
        fd.flip1 = flip1 ? -1 : 1;
        _faces.append(fd);
        _ready = true;
    }

}

void SoftskinNormal::update() {

    if (!_ready) {
        return;
    }

    int imax = _faces.size();
    _normal *= 0;
    
    Vector3 d0;
    Vector3 d1;
    Vector3 n;
    
    for (int i = 0; i < imax; ++i) {
        const fdata& fd = _faces[i];
        d0 = fd.edge0->get_dir() * fd.flip0;
        d1 = fd.edge1->get_dir() * fd.flip1;
        n = d0.cross(d1);
        n.normalize();
        _normal += n;
    }
    _normal.normalize();
    
    // pushing to dots
    for (int i = 0; i < imax; ++i) {
        _faces[i].dot->add_normal( _normal );
    }

}

const Vector3& SoftskinNormal::get_normal() const {
    return _normal;
}

const real_t* SoftskinNormal::coord() const {
    return _normal.coord;
}