/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   soft_mesh.h
 * Author: frankiezafe
 *
 * Created on July 20, 2019, 11:43 PM
 */

#ifndef SOFTSKIN_MESH_H
#define SOFTSKIN_MESH_H

#include "softskin_resource.h"
#include "softskin_cache.h"

class SoftskinMesh : public SkinResource {
    GDCLASS(SoftskinMesh, SkinResource);
    OBJ_CATEGORY("Softskin");

public:

    SoftskinMesh();
    virtual ~SoftskinMesh();

    void purge();

    Error load_file(const String &p_path);

    _FORCE_INLINE_ bool is_ready() const {
        return _dots.size() != 0 && 
                _fibers.size() != 0 &&
                _indices.size() != 0;
    }
    
    void dump( bool deep = false );
    
    Ref<SoftskinCache> get_cache();

protected:
    
    Ref<SoftskinCache> _cache;

    // skin specific
    PoolVector<Vector3> _anchors;
    PoolVector<Vector3> _dots;
    PoolVector<int32_t> _face_fibers;
    PoolVector<int32_t> _fibers;
    PoolVector<int32_t> _ligaments;
    std::vector<group_ref> _groups;
    PoolVector< PoolVector<Vector2> > _extra_uvs;
    // godot mesh related >> data is ready to dump in memory
    PoolVector<Vector3> _vertices;
    PoolVector<Vector3> _normals;
    PoolVector<real_t> _tangents;
    PoolVector<Color> _colors;
    PoolVector<Vector2> _uvs;
    PoolVector<Vector2> _uv2s;
    PoolVector<int32_t> _indices;
    // fibers to use for normal computation
    std::vector< normal_ref > _unique_normals;

    static void _bind_methods();

    bool deserialise(FileAccess* f);
    
};

#endif /* SOFTSKIN_MESH_H */

