/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_cache.cpp
 * Author: frankiezafe
 * 
 * Created on July 16, 2019, 1:19 PM
 */

#include "softskin_cache.h"
#include "thirdparty/bullet/LinearMath/btAlignedAllocator.h"

SoftskinCache::SoftskinCache() {
    purge();
}

SoftskinCache::~SoftskinCache() {
    purge();
}

void SoftskinCache::_bind_methods() {
}

Error SoftskinCache::save_file(const String &p_path) {

    Error error_file;
    FileAccess* f = FileAccess::open(p_path, FileAccess::WRITE, &error_file);

    if (!f) {
        std::cout << "SSC, write: cannot access file..." << std::endl;
        return FAILED;
    }

    f->seek(0);
    serialise(f);
    f->close();
    memdelete(f);

    return OK;

}

Error SoftskinCache::load_file(const String &p_path) {

    Error error_file;
    FileAccess* f = FileAccess::open(p_path, FileAccess::READ, &error_file);

    if (!f) {
        std::cout << "SSC, read: cannot access file..." << std::endl;
        return FAILED;
    }

    // back to beginning
    f->seek(0);

    std::string smarker;
    uint32_t imarker;
    load_value(f, smarker);
    load_value(f, imarker);

    bool valid = true;

    if (smarker.compare(SSC_EXT) == -1) {
        std::cout << "SoftskinCache::load_file, WRONG FILE TYPE!" << std::endl;
        valid = false;
    } else if (imarker != SOFTSKIN_VERSION) {
        std::cout << "SoftskinCache::load_file, " << imarker << " WRONG VERSION, currently supported " << SOFTSKIN_VERSION << std::endl;
        valid = false;
    }

    if (valid) {
        purge();
        if (!deserialise(f)) {
            std::cout << "SoftskinCache::load_file, PARSING WENT WRONG!" << std::endl;
            purge();
        }
    }

    f->close();
    memdelete(f);

    emit_changed();

    return OK;

}

void SoftskinCache::purge() {

    purge_analysis();

    std::vector<MeshArray*>::iterator it = _surfaces.begin();
    std::vector<MeshArray*>::iterator ite = _surfaces.end();
    for (; it != ite; ++it) {
        memdelete((*it));
    }
    _surfaces.resize(0);

}

void SoftskinCache::purge_analysis() {

    _all2unique.clear();
    _unique_dots.clear();
    _unique_anchors.clear();
    _unique_fibers.clear();
    _unique_ligaments.clear();
    _unique_normals.clear();
    _unique_normals.clear();

    _anchors.resize(0);
    _groups.clear();

    _face_all_verts.clear();
    _face_fibers.clear();

}

void SoftskinCache::load(Ref<Mesh> m) {

    purge();

    if (m.is_null()) {
        return;
    }

    uint32_t max_array = VS::ARRAY_MAX;
    uint32_t snum = m->get_surface_count();

    _surfaces.resize(snum);

    for (uint32_t i = 0; i < snum; ++i) {

        MeshArray* ma = memnew(MeshArray);
        _surfaces[i] = ma;

        ma->surface_format = VS::get_singleton()->mesh_surface_get_format(m->get_rid(), i);
        ma->primitive_type = VS::get_singleton()->mesh_surface_get_primitive_type(m->get_rid(), i);

        const uint32_t surface_vertex_len = VS::get_singleton()->mesh_surface_get_array_len(m->get_rid(), i);
        const uint32_t surface_index_len = VS::get_singleton()->mesh_surface_get_array_index_len(m->get_rid(), i);
        uint32_t surface_offsets[VS::ARRAY_MAX];

        ma->stride = VS::get_singleton()->mesh_surface_make_offsets_from_format(ma->surface_format, surface_vertex_len, surface_index_len, surface_offsets);
        ma->offset_vertices = surface_offsets[VS::ARRAY_VERTEX];
        ma->offset_normal = surface_offsets[VS::ARRAY_NORMAL];
        ma->offset_uv = surface_offsets[VS::ARRAY_TEX_UV];

        ma->buffer = VS::get_singleton()->mesh_surface_get_array(m->get_rid(), i);

        ma->arr = m->surface_get_arrays(i);

        for (uint32_t j = 0; j < max_array; ++j) {
            VS::ArrayType t = VS::ArrayType(j);
            switch (t) {
                case VS::ARRAY_VERTEX:
                {
                    ma->verts = ma->arr[t];
                }
                    break;
                case VS::ARRAY_NORMAL:
                {
                    ma->normals = ma->arr[t];
                }
                case VS::ARRAY_INDEX:
                {
                    ma->indices = ma->arr[t];
                }
                    break;
                default:
                    break;
            }
        }

    }

}

bool SoftskinCache::compare(const SoftskinCache* src) {

    if (!src) {
        return false;
    }

    if (src->_surfaces.size() != _surfaces.size()) {
        return false;
    }

    for (size_t i = 0, imax = _surfaces.size(); i < imax; ++i) {
        MeshArray* foreign_ma = src->_surfaces[i];
        if (!SkinResource::compare(_surfaces[i]->verts, foreign_ma->verts)) {
            return false;
        }
        if (!SkinResource::compare(_surfaces[i]->indices, foreign_ma->indices)) {
            return false;
        }
    }

    return true;

}

void SoftskinCache::copy_bufferinfo(SoftskinData* dst, const size_t& surfID) {

    if (surfID >= _surfaces.size()) {
        return;
    }
    MeshArray* ma = _surfaces[surfID];
    dst->surface_format = ma->surface_format;
    dst->primitive_type = ma->primitive_type;
    dst->stride = ma->stride;
    dst->offset_vertices = ma->offset_vertices;
    dst->offset_normal = ma->offset_normal;
    dst->offset_uv = ma->offset_uv;

}

void SoftskinCache::dump() {

    std::cout << "SoftskinCache dump ********************** " << std::endl;
    std::cout << "\tsurfaces: " << _surfaces.size() << std::endl;
    for (size_t i = 0; i < _surfaces.size(); ++i) {
        std::cout << "\t\t**" << i << " ***" << std::endl;
        std::cout << "\t\t" << "surface_format: " << _surfaces[i]->surface_format << std::endl;
        std::cout << "\t\t" << "primitive_type: " << _surfaces[i]->primitive_type << std::endl;
        std::cout << "\t\t" << "stride: " << _surfaces[i]->stride << std::endl;
        std::cout << "\t\t" << "offset_vertices: " << _surfaces[i]->offset_vertices << std::endl;
        std::cout << "\t\t" << "offset_normal: " << _surfaces[i]->offset_normal << std::endl;
        std::cout << "\t\t" << "offset_uv: " << _surfaces[i]->offset_uv << std::endl;
        std::cout << "\t\t" << "buffer: " << _surfaces[i]->buffer.size() << std::endl;
        std::cout << "\t\t" << "verts: " << _surfaces[i]->verts.size() << std::endl;
        std::cout << "\t\t" << "normals: " << _surfaces[i]->normals.size() << std::endl;
        std::cout << "\t\t" << "indices: " << _surfaces[i]->indices.size() << std::endl;
    }
    std::cout << "\t" << "_all2unique: " << _all2unique.size() << std::endl;
    std::cout << "\t" << "_unique_vertices: " << _unique_dots.size() << std::endl;
    std::cout << "\t" << "_unique_anchors: " << _unique_anchors.size() << std::endl;
    std::cout << "\t" << "_unique_fibers: " << _unique_fibers.size() << std::endl;
    std::cout << "\t" << "_unique_ligaments: " << _unique_ligaments.size() << std::endl;
    std::cout << "\t" << "_face_all_verts: " << _face_all_verts.size() << std::endl;
    std::cout << "\t" << "_face_edges: " << _face_fibers.size() << std::endl;
    std::cout << "\t" << "_unique_normals: " << _unique_normals.size() << std::endl;

}

void SoftskinCache::dump_arr(const Array& a) {

    array_decompressor arrdc;
    array_to_decompressor(a, arrdc);
    std::cout << "Array dump" << std::endl;
    std::cout << "\t" << "ARRAY_VERTEX: " << arrdc.ARRAY_VERTEX.size() << std::endl;
    std::cout << "\t" << "ARRAY_NORMAL: " << arrdc.ARRAY_NORMAL.size() << std::endl;
    std::cout << "\t" << "ARRAY_TANGENT: " << arrdc.ARRAY_TANGENT.size() << std::endl;
    std::cout << "\t" << "ARRAY_COLOR: " << arrdc.ARRAY_COLOR.size() << std::endl;
    std::cout << "\t" << "ARRAY_TEX_UV: " << arrdc.ARRAY_TEX_UV.size() << std::endl;
    std::cout << "\t" << "ARRAY_TEX_UV2: " << arrdc.ARRAY_TEX_UV2.size() << std::endl;
    std::cout << "\t" << "ARRAY_BONES: " << arrdc.ARRAY_BONES.size() << std::endl;
    std::cout << "\t" << "ARRAY_WEIGHTS: " << arrdc.ARRAY_WEIGHTS.size() << std::endl;
    std::cout << "\t" << "ARRAY_INDEX: " << arrdc.ARRAY_INDEX.size() << std::endl;

}

void SoftskinCache::save_surfaces(FileAccess* f) {

    // verts & indices are not stored as the are copies of data in ARRAY

    save_marker(f, SSC_SURFACEMAR);
    // num of surfaces
    save_value(f, _surfaces.size());
    // looping over surfaces
    std::vector<MeshArray*>::iterator it = _surfaces.begin();
    std::vector<MeshArray*>::iterator ite = _surfaces.end();
    for (; it != ite; ++it) {

        MeshArray* ma = (*it);
        // uints
        /* 00 */ save_value(f, ma->surface_format);
        /* 01 */ save_value(f, ma->primitive_type);
        /* 02 */ save_value(f, ma->stride);
        /* 03 */ save_value(f, ma->offset_vertices);
        /* 04 */ save_value(f, ma->offset_normal);
        /* 05 */ save_value(f, ma->offset_uv);
        // counts
        /* 06 */ save_value(f, size_t(ma->buffer.size()));
        /* 07 */ save_value(f, size_t(ma->verts.size()));
        /* 08 */ save_value(f, size_t(ma->normals.size()));
        /* 09 */ save_value(f, size_t(ma->indices.size()));
        // dumping pollvectors
        /* 10 */ save_poolvector(f, ma->buffer);
        /* 11 */ save_poolvector(f, ma->verts);
        /* 12 */ save_poolvector(f, ma->normals);
        /* 13 */ save_poolvector(f, ma->indices);
        f->flush();
        // finishing with array
        /* 14 */ save_array(f, ma->arr);
    }
    save_marker(f, SSC_END_MARKER);

}

bool SoftskinCache::load_surfaces(FileAccess* f) {

    uint32_t buffs = 0;
    uint32_t verts = 0;
    uint32_t norms = 0;
    uint32_t indis = 0;
    size_t surfcount = 0;
    load_value(f, surfcount);

    _surfaces.resize(surfcount);
    for (size_t i = 0; i < surfcount; ++i) {

        MeshArray* ma = memnew(MeshArray);
        _surfaces[i] = ma;
        // uints
        /* 00 */ load_value(f, ma->surface_format);
        /* 01 */ load_value(f, ma->primitive_type);
        /* 02 */ load_value(f, ma->stride);
        /* 03 */ load_value(f, ma->offset_vertices);
        /* 04 */ load_value(f, ma->offset_normal);
        /* 05 */ load_value(f, ma->offset_uv);
        // counts
        /* 06 */ load_value(f, buffs);
        /* 07 */ load_value(f, verts);
        /* 08 */ load_value(f, norms);
        /* 09 */ load_value(f, indis);
        // getting pollvectors
        /* 10 */ load_poolvector(f, buffs, ma->buffer);
        /* 11 */ load_poolvector(f, verts, ma->verts);
        /* 12 */ load_poolvector(f, norms, ma->normals);
        /* 13 */ load_poolvector(f, indis, ma->indices);
        // checking the marker for array
        if (!is_marker(f, SSC_ARRAYMARKE)) {
            return false;
        }
        /* 14 */ if (!load_array(f, ma->arr)) {
            return false;
        }

    }
    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    return true;

}

void SoftskinCache::save_all2unique(FileAccess* f) {

    ss_unique_vec tmp;
    tmp.resize(_all2unique.size() * 2);
    ss_unique_map::iterator it = _all2unique.begin();
    ss_unique_map::iterator ite = _all2unique.end();
    size_t ti = 0;
    for (; it != ite; ++it) {
        tmp[ti++] = it->first;
        tmp[ti++] = it->second;
    }
    save_marker(f, SSC_ALL2UNIQUE);
    save_value(f, ti);
    save_stdvector(f, tmp);
    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_all2unique(FileAccess* f) {

    size_t i;
    load_value(f, i);
    ss_unique_vec tmp;
    load_stdvector(f, i, tmp);
    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    ss_unique_vec::iterator it = tmp.begin();
    ss_unique_vec::iterator ite = tmp.end();
    for (; it != ite;) {
        uint32_t k = (*it);
        ++it;
        uint32_t v = (*it);
        ++it;
        _all2unique[k] = v;
    }
    return true;

}

void SoftskinCache::save_unique_dots(FileAccess* f) {

    save_marker(f, SSC_UNIQUEDOTS);
    save_value(f, _unique_dots.size());

    std::vector<v3_ref>::iterator it = _unique_dots.begin();
    std::vector<v3_ref>::iterator ite = _unique_dots.end();

    for (; it != ite; ++it) {
        v3_ref& vr = (*it);
        uint32_t b2f = 0;
        assert(vr.local_id >= 0);
        if (vr.belong_2_face) {
            b2f = 1;
        }
        save_value(f, b2f);
        save_value(f, uint32_t(vr.local_id));
        save_value(f, vr.xyz.x);
        save_value(f, vr.xyz.y);
        save_value(f, vr.xyz.z);
        uint32_t mnum = vr.mirrors.size();
        save_value(f, mnum);
        for (uint32_t i = 0; i < mnum; ++i) {
            assert(vr.mirrors[i] >= 0);
            save_value(f, uint32_t(vr.mirrors[i]));
        }
    }

    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_unique_dots(FileAccess* f) {

    size_t uvnum;
    load_value(f, uvnum);
    _unique_dots.resize(uvnum);

    uint32_t b2f;
    uint32_t mnum;
    uint32_t mi;
    std::vector<v3_ref>::iterator it = _unique_dots.begin();
    std::vector<v3_ref>::iterator ite = _unique_dots.end();
    for (; it != ite; ++it) {
        v3_ref& vr = (*it);
        load_value(f, b2f);
        if (b2f == 1) {
            vr.belong_2_face = true;
        } else {
            vr.belong_2_face = false;
        }
        load_value(f, vr.local_id);
        load_value(f, vr.xyz.x);
        load_value(f, vr.xyz.y);
        load_value(f, vr.xyz.z);
        load_value(f, mnum);
        vr.mirrors.resize(0);
        for (uint32_t i = 0; i < mnum; ++i) {
            load_value(f, mi);
            vr.mirrors.push_back(mi);
        }
    }

    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    return true;

}

void SoftskinCache::save_unique_anchors(FileAccess* f) {

    save_marker(f, SSC_UNIQUEANCH);
    save_value(f, _unique_anchors.size());

    std::vector<v3_ref>::iterator it = _unique_anchors.begin();
    std::vector<v3_ref>::iterator ite = _unique_anchors.end();

    for (; it != ite; ++it) {
        v3_ref& vr = (*it);
        uint32_t b2f = 0;
        if (vr.belong_2_face) {
            b2f = 1;
        }
        save_value(f, b2f);
        save_value(f, vr.local_id);
        save_value(f, vr.xyz.x);
        save_value(f, vr.xyz.y);
        save_value(f, vr.xyz.z);
        uint32_t mnum = vr.mirrors.size();
        save_value(f, mnum);
        for (uint32_t i = 0; i < mnum; ++i) {
            save_value(f, vr.mirrors[i]);
        }
    }

    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_unique_anchors(FileAccess* f) {

    size_t uvnum;
    load_value(f, uvnum);
    _unique_anchors.resize(uvnum);

    uint32_t b2f;
    uint32_t mnum;
    uint32_t mi;
    std::vector<v3_ref>::iterator it = _unique_anchors.begin();
    std::vector<v3_ref>::iterator ite = _unique_anchors.end();
    for (; it != ite; ++it) {
        v3_ref& vr = (*it);
        load_value(f, b2f);
        if (b2f == 1) {
            vr.belong_2_face = true;
        } else {
            vr.belong_2_face = false;
        }
        load_value(f, vr.local_id);
        load_value(f, vr.xyz.x);
        load_value(f, vr.xyz.y);
        load_value(f, vr.xyz.z);
        load_value(f, mnum);
        vr.mirrors.resize(0);
        for (uint32_t i = 0; i < mnum; ++i) {
            load_value(f, mi);
            vr.mirrors.push_back(mi);
        }
    }

    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    return true;

}

void SoftskinCache::save_unique_fibers(FileAccess* f) {

    save_marker(f, SSC_UNIQUEFIBE);
    save_value(f, _unique_fibers.size());

    std::vector<edge_ref>::iterator it = _unique_fibers.begin();
    std::vector<edge_ref>::iterator ite = _unique_fibers.end();

    for (; it != ite; ++it) {
        edge_ref& er = (*it);
        save_value(f, er.v3_ref_0);
        save_value(f, er.v3_ref_1);
        uint32_t lig = 0;
        if (er.ligament) {
            lig = 1;
        }
        save_value(f, lig);
        uint32_t mnum = er.faces.size();
        save_value(f, mnum);
        for (uint32_t i = 0; i < mnum; ++i) {
            save_value(f, er.faces[i]);
        }
    }

    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_unique_fibers(FileAccess* f) {

    size_t uvnum;
    load_value(f, uvnum);
    _unique_fibers.resize(uvnum);

    uint32_t b2f;
    uint32_t mnum;
    uint32_t mi;
    std::vector<edge_ref>::iterator it = _unique_fibers.begin();
    std::vector<edge_ref>::iterator ite = _unique_fibers.end();
    for (; it != ite; ++it) {
        edge_ref& er = (*it);
        load_value(f, er.v3_ref_0);
        load_value(f, er.v3_ref_1);
        load_value(f, b2f);
        if (b2f == 0) {
            er.ligament = false;
        } else {
            er.ligament = true;
        }
        load_value(f, mnum);
        er.faces.resize(0);
        for (uint32_t i = 0; i < mnum; ++i) {
            load_value(f, mi);
            er.faces.push_back(mi);
        }
    }

    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    return true;

}

void SoftskinCache::save_unique_ligaments(FileAccess* f) {

    save_marker(f, SSC_UNIQUELIGA);
    save_value(f, _unique_ligaments.size());

    std::vector<edge_ref>::iterator it = _unique_ligaments.begin();
    std::vector<edge_ref>::iterator ite = _unique_ligaments.end();

    for (; it != ite; ++it) {
        edge_ref& er = (*it);
        save_value(f, er.v3_ref_0);
        save_value(f, er.v3_ref_1);
        uint32_t lig = 0;
        if (er.ligament) {
            lig = 1;
        }
        save_value(f, lig);
        uint32_t mnum = er.faces.size();
        save_value(f, mnum);
        for (uint32_t i = 0; i < mnum; ++i) {
            save_value(f, er.faces[i]);
        }
    }

    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_unique_ligaments(FileAccess* f) {

    size_t uvnum;
    load_value(f, uvnum);
    _unique_ligaments.resize(uvnum);

    uint32_t b2f;
    uint32_t mnum;
    uint32_t mi;
    std::vector<edge_ref>::iterator it = _unique_ligaments.begin();
    std::vector<edge_ref>::iterator ite = _unique_ligaments.end();
    for (; it != ite; ++it) {
        edge_ref& er = (*it);
        load_value(f, er.v3_ref_0);
        load_value(f, er.v3_ref_1);
        load_value(f, b2f);
        if (b2f == 0) {
            er.ligament = false;
        } else {
            er.ligament = true;
        }
        load_value(f, mnum);
        er.faces.resize(0);
        for (uint32_t i = 0; i < mnum; ++i) {
            load_value(f, mi);
            er.faces.push_back(mi);
        }
    }

    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    return true;

}

void SoftskinCache::save_face_map(FileAccess* f, ss_face_map& fm) {

    save_value(f, fm.size());

    ss_face_map::iterator it = fm.begin();
    ss_face_map::iterator ite = fm.end();

    for (; it != ite; ++it) {
        save_value(f, it->first);
        save_value(f, it->second.size());
        save_stdvector(f, it->second);
    }

    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_face_map(FileAccess* f, ss_face_map& fm) {

    size_t fnum;
    load_value(f, fnum);

    uint32_t k;
    size_t vnum;
    size_t i = 0;
    while (i < fnum) {
        load_value(f, k);
        load_value(f, vnum);
        fm[k];
        load_stdvector(f, vnum, fm[k]);
        ++i;
    }

    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    return true;

}

void SoftskinCache::save_unique_normals(FileAccess* f) {

    save_marker(f, SSC_UNIQUENORM);
    save_value(f, _unique_normals.size());

    std::vector<normal_ref>::iterator it = _unique_normals.begin();
    std::vector<normal_ref>::iterator ite = _unique_normals.end();

    uint32_t flip;
    for (; it != ite; ++it) {
        normal_ref& nr = (*it);
        save_value(f, nr.dot_id);
        uint32_t fn = nr.faces.size();
        save_value(f, fn);
        for (uint32_t i = 0; i < fn; ++i) {
            save_value(f, nr.faces[i]);
        }
        save_value(f, nr.edges.size());
        std::vector<two_edge>::iterator et = nr.edges.begin();
        std::vector<two_edge>::iterator ete = nr.edges.end();
        for (; et != ete; ++et) {
            two_edge& to = (*et);
            save_value(f, to.buffer_id);
            save_value(f, to.edge0);
            save_value(f, to.edge1);
            flip = 0;
            if (to.flip0) {
                flip = 1;
            }
            save_value(f, flip);
            flip = 0;
            if (to.flip1) {
                flip = 1;
            }
            save_value(f, flip);
        }
    }

    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_unique_normals(FileAccess* f) {

    size_t unn;
    load_value(f, unn);
    _unique_normals.resize(unn);

    uint32_t fn;
    uint32_t fid;
    size_t en;
    uint32_t flip;
    std::vector<normal_ref>::iterator it = _unique_normals.begin();
    std::vector<normal_ref>::iterator ite = _unique_normals.end();
    for (; it != ite; ++it) {
        normal_ref& nr = (*it);
        load_value(f, nr.dot_id);
        load_value(f, fn);
        for (uint32_t i = 0; i < fn; ++i) {
            load_value(f, fid);
            nr.faces.push_back(fid);
        }
        load_value(f, en);
        nr.edges.resize(en);
        std::vector<two_edge>::iterator et = nr.edges.begin();
        std::vector<two_edge>::iterator ete = nr.edges.end();
        for (; et != ete; ++et) {
            two_edge& to = (*et);
            load_value(f, to.buffer_id);
            load_value(f, to.edge0);
            load_value(f, to.edge1);
            load_value(f, flip);
            if (flip == 0) {
                to.flip0 = false;
            } else {
                to.flip0 = true;
            }
            load_value(f, flip);
            if (flip == 0) {
                to.flip1 = false;
            } else {
                to.flip1 = true;
            }
        }
    }

    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    return true;

}

void SoftskinCache::save_anchors(FileAccess* f) {

    save_marker(f, SSC_ANCHORMARK);
    save_value(f, size_t(_anchors.size()));
    save_poolvector(f, _anchors);
    save_marker(f, SSC_END_MARKER);
    f->flush();

}

bool SoftskinCache::load_anchors(FileAccess* f) {

    uint32_t anum;
    load_value(f, anum);
    load_poolvector(f, anum, _anchors);

    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "load_anchors" << std::endl;
        return false;
    }

    return true;

}

void SoftskinCache::save_groups(FileAccess* f) {

    save_marker(f, SSC_GROUPMARKE);
    save_value(f, _groups.size());

    std::vector<group_ref>::iterator itg = _groups.begin();
    std::vector<group_ref>::iterator itge = _groups.end();
    for (; itg != itge; ++itg) {

        group_ref& gr = (*itg);

        save_value(f, gr.name.c_str());
        save_value(f, uint32_t(gr.type));

        save_value(f, size_t(gr.indices.size()));
        save_poolvector(f, gr.indices);

        save_value(f, size_t(gr.weights.size()));
        save_poolvector(f, gr.weights);

        save_marker(f, SSC_END_MARKER);

    }

    save_marker(f, SSC_END_MARKER);

    f->flush();

}

bool SoftskinCache::load_groups(FileAccess* f) {

    size_t gnum;
    load_value(f, gnum);

    _groups.resize(gnum);

    for (size_t i = 0; i < gnum; ++i) {

        group_ref& gr = _groups[i];
        load_value(f, gr.name);

        uint32_t t;
        load_value(f, t);
        gr.type = softskin_type_t(t);

        uint32_t vsize;

        load_value(f, vsize);
        load_poolvector(f, vsize, gr.indices);

        load_value(f, vsize);
        load_poolvector(f, vsize, gr.weights);

        if (!is_marker(f, SSC_END_MARKER)) {
            std::cout << "load_groups " << i << std::endl;
            return false;
        }

    }

    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "load_groups end" << std::endl;
        return false;
    }

    return true;

}

void SoftskinCache::serialise(FileAccess* f) {

    // back to beginning
    f->seek(0);

    // file header: 
    // - [0,254]: scc ( SOFSKINCACHE_EXT )
    // - [255,258]: 100 ( SOFTSKIN_VERSION )

    save_value(f, SSC_EXT);
    save_value(f, int(SOFTSKIN_VERSION));

    f->flush();

    save_surfaces(f);

    save_all2unique(f);

    save_unique_dots(f);

    save_unique_anchors(f);

    save_unique_fibers(f);

    save_unique_ligaments(f);

    save_unique_normals(f);

    save_anchors(f);

    save_groups(f);

    save_marker(f, SSC_FACEALLVER);
    save_face_map(f, _face_all_verts);

    save_marker(f, SSC_FACEFIBERM);
    save_face_map(f, _face_fibers);

}

bool SoftskinCache::deserialise(FileAccess* f) {

    int unsuccessfull_attempt = 0;
    // next one should be a marker
    while (unsuccessfull_attempt < 5 && f->get_position() < f->get_len()) {

        uint8_t us[SSC_MARKER_SIZE];
        f->get_buffer(us, SSC_MARKER_SIZE);

        if (is_marker(us, SSC_SURFACEMAR)) {
            if (!load_surfaces(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_ALL2UNIQUE)) {
            if (!load_all2unique(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_UNIQUEDOTS)) {
            if (!load_unique_dots(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_UNIQUEANCH)) {
            if (!load_unique_anchors(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_UNIQUEFIBE)) {
            if (!load_unique_fibers(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_UNIQUELIGA)) {
            if (!load_unique_ligaments(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_FACEALLVER)) {
            if (!load_face_map(f, _face_all_verts)) {
                return false;
            }
        } else if (is_marker(us, SSC_FACEFIBERM)) {
            if (!load_face_map(f, _face_fibers)) {
                return false;
            }
        } else if (is_marker(us, SSC_UNIQUENORM)) {
            if (!load_unique_normals(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_ANCHORMARK)) {
            if (!load_anchors(f)) {
                return false;
            }
        } else if (is_marker(us, SSC_GROUPMARKE)) {
            if (!load_groups(f)) {
                return false;
            }
        } else {
            unsuccessfull_attempt++;
        }
    }

    return true;

}

void SoftskinCache::save_array(FileAccess* f, Array& a) {

    save_marker(f, SSC_ARRAYMARKE);

    array_decompressor arrc;

    // collecting data
    uint32_t max_array = VS::ARRAY_MAX;
    for (uint32_t j = 0; j < max_array; ++j) {
        VS::ArrayType t = VS::ArrayType(j);
        switch (t) {
            case VS::ARRAY_VERTEX:
                arrc.ARRAY_VERTEX = a[t];
                break;
            case VS::ARRAY_NORMAL:
                arrc.ARRAY_NORMAL = a[t];
                break;
            case VS::ARRAY_TANGENT:
                arrc.ARRAY_TANGENT = a[t];
                break;
            case VS::ARRAY_COLOR:
                arrc.ARRAY_COLOR = a[t];
                break;
            case VS::ARRAY_TEX_UV:
                arrc.ARRAY_TEX_UV = a[t];
                break;
            case VS::ARRAY_TEX_UV2:
                arrc.ARRAY_TEX_UV2 = a[t];
                break;
            case VS::ARRAY_BONES:
                arrc.ARRAY_BONES = a[t];
                break;
            case VS::ARRAY_WEIGHTS:
                arrc.ARRAY_WEIGHTS = a[t];
                break;
            case VS::ARRAY_INDEX:
                arrc.ARRAY_INDEX = a[t];
                break;
            default:
                break;
        }
    }

    // dumping length of pools
    /* 00 */ save_value(f, arrc.ARRAY_VERTEX.size());
    /* 01 */ save_value(f, arrc.ARRAY_NORMAL.size());
    /* 02 */ save_value(f, arrc.ARRAY_TANGENT.size());
    /* 03 */ save_value(f, arrc.ARRAY_COLOR.size());
    /* 04 */ save_value(f, arrc.ARRAY_TEX_UV.size());
    /* 05 */ save_value(f, arrc.ARRAY_TEX_UV2.size());
    /* 06 */ save_value(f, arrc.ARRAY_BONES.size());
    /* 07 */ save_value(f, arrc.ARRAY_WEIGHTS.size());
    /* 08 */ save_value(f, arrc.ARRAY_INDEX.size());
    // and dumping the pollvectors
    /* 09 */ save_poolvector(f, arrc.ARRAY_VERTEX);
    /* 10 */ save_poolvector(f, arrc.ARRAY_NORMAL);
    /* 11 */ save_poolvector(f, arrc.ARRAY_TANGENT);
    /* 12 */ save_poolvector(f, arrc.ARRAY_COLOR);
    /* 13 */ save_poolvector(f, arrc.ARRAY_TEX_UV);
    /* 14 */ save_poolvector(f, arrc.ARRAY_TEX_UV2);
    /* 15 */ save_poolvector(f, arrc.ARRAY_BONES);
    /* 16 */ save_poolvector(f, arrc.ARRAY_WEIGHTS);
    /* 17 */ save_poolvector(f, arrc.ARRAY_INDEX);

    save_marker(f, SSC_END_MARKER);

    f->flush();

}

bool SoftskinCache::load_array(FileAccess* f, Array& a) {

    array_decompressor arrc;

    uint32_t venum;
    uint32_t nonum;
    uint32_t tanum;
    uint32_t conum;
    uint32_t tunum;
    uint32_t t2num;
    uint32_t bonum;
    uint32_t wenum;
    uint32_t innum;

    // getting length of pools
    /* 00 */ load_value(f, venum);
    /* 01 */ load_value(f, nonum);
    /* 02 */ load_value(f, tanum);
    /* 03 */ load_value(f, conum);
    /* 04 */ load_value(f, tunum);
    /* 05 */ load_value(f, t2num);
    /* 06 */ load_value(f, bonum);
    /* 07 */ load_value(f, wenum);
    /* 08 */ load_value(f, innum);
    // and dumping the pollvectors
    /* 09 */ load_poolvector(f, venum, arrc.ARRAY_VERTEX);
    /* 10 */ load_poolvector(f, nonum, arrc.ARRAY_NORMAL);
    /* 11 */ load_poolvector(f, tanum, arrc.ARRAY_TANGENT);
    /* 12 */ load_poolvector(f, conum, arrc.ARRAY_COLOR);
    /* 13 */ load_poolvector(f, tunum, arrc.ARRAY_TEX_UV);
    /* 14 */ load_poolvector(f, t2num, arrc.ARRAY_TEX_UV2);
    /* 15 */ load_poolvector(f, bonum, arrc.ARRAY_BONES);
    /* 16 */ load_poolvector(f, wenum, arrc.ARRAY_WEIGHTS);
    /* 17 */ load_poolvector(f, innum, arrc.ARRAY_INDEX);

    // checking end marker
    if (!is_marker(f, SSC_END_MARKER)) {
        return false;
    }

    decompressor_to_array(arrc, a);

    return true;

}

void SoftskinCache::decompressor_to_array(const array_decompressor& arrc, Array& a) {

    uint32_t max_array = VS::ARRAY_MAX;
    a.resize(max_array);
    for (uint32_t j = 0; j < max_array; ++j) {
        VS::ArrayType t = VS::ArrayType(j);
        switch (t) {
            case VS::ARRAY_VERTEX:
                a[t] = arrc.ARRAY_VERTEX;
                break;
            case VS::ARRAY_NORMAL:
                if (arrc.ARRAY_NORMAL.size() != 0)
                    a[t] = arrc.ARRAY_NORMAL;
                break;
            case VS::ARRAY_TANGENT:
                if (arrc.ARRAY_TANGENT.size() != 0)
                    a[t] = arrc.ARRAY_TANGENT;
                break;
            case VS::ARRAY_COLOR:
                if (arrc.ARRAY_COLOR.size() != 0)
                    a[t] = arrc.ARRAY_COLOR;
                break;
            case VS::ARRAY_TEX_UV:
                if (arrc.ARRAY_TEX_UV.size() != 0)
                    a[t] = arrc.ARRAY_TEX_UV;
                break;
            case VS::ARRAY_TEX_UV2:
                if (arrc.ARRAY_TEX_UV2.size() != 0)
                    a[t] = arrc.ARRAY_TEX_UV2;
                break;
            case VS::ARRAY_BONES:
                if (arrc.ARRAY_BONES.size() != 0)
                    a[t] = arrc.ARRAY_BONES;
                break;
            case VS::ARRAY_WEIGHTS:
                if (arrc.ARRAY_WEIGHTS.size() != 0)
                    a[t] = arrc.ARRAY_WEIGHTS;
                break;
            case VS::ARRAY_INDEX:
                a[t] = arrc.ARRAY_INDEX;
                break;
            default:
                break;
        }
    }

}

void SoftskinCache::array_to_decompressor(const Array& a, array_decompressor& arrc) {

    uint32_t max_array = VS::ARRAY_MAX;
    for (uint32_t j = 0; j < max_array; ++j) {
        VS::ArrayType t = VS::ArrayType(j);
        switch (t) {
            case VS::ARRAY_VERTEX:
                arrc.ARRAY_VERTEX = a[t];
                break;
            case VS::ARRAY_NORMAL:
                arrc.ARRAY_NORMAL = a[t];
                break;
            case VS::ARRAY_TANGENT:
                arrc.ARRAY_TANGENT = a[t];
                break;
            case VS::ARRAY_COLOR:
                arrc.ARRAY_COLOR = a[t];
                break;
            case VS::ARRAY_TEX_UV:
                arrc.ARRAY_TEX_UV = a[t];
                break;
            case VS::ARRAY_TEX_UV2:
                arrc.ARRAY_TEX_UV2 = a[t];
                break;
            case VS::ARRAY_BONES:
                arrc.ARRAY_BONES = a[t];
                break;
            case VS::ARRAY_WEIGHTS:
                arrc.ARRAY_WEIGHTS = a[t];
                break;
            case VS::ARRAY_INDEX:
                arrc.ARRAY_INDEX = a[t];
                break;
            default:
                break;
        }
    }

}
