# run this script to install icons in the editor folder - will require a recompilation

import os
from shutil import copyfile

icons_target = "../../editor/icons"
icons_source = "icons"

icons = [f for f in os.listdir(icons_source) if os.path.isfile(os.path.join(icons_source, f))]

for i in icons:
	src = os.path.join(icons_source, i)
	trg = os.path.join(*icons_target.split('/'), i)
	copyfile(src, trg)
	print( "copy", src, "to", trg )