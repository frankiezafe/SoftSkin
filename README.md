# SoftSkin

![softskin_12_shivinteger](https://polymorph.cool/wp-content/uploads/2019/08/softskin_12_shivinteger.png)

Softskin is an addon for [godot engine](http://godotengine.org).

It is based on [tensegrity](https://en.wikipedia.org/wiki/Tensegrity), using the edges of the meshes to diffuse forces across the mesh. It can be used with standard models, but some advanced features are only availables by using the custom file format (SSM).

Development is at a early stage, so there is no documentation yet (some features are a bit tricky to setup).

Video devlogs:

* [softskin devlog #012](https://peertube.mastodon.host/videos/watch/578d928e-256e-43eb-b23e-7fe4c8b4b7f9)
* [softskin experiment #011](https://peertube.mastodon.host/videos/watch/6e5ce3bf-0733-422b-89a2-8184070ce476)
* [softskin #010](https://peertube.mastodon.host/videos/watch/d7867c37-737f-465d-9ad8-b071c0672a89)

## blender addons

a specific exporter allows configuration of meshes in blender (a complete documentation will follow, check blends/arm.blend in demo project for example)

the file extension is 'ssm', meaning softskin mesh

- 2.7x: assets/blender/2.79/io_softskin.zip
- 2.80: assets/blender/2.80/io_softskin.zip

another simple tool is available to split quads in 4 triangles

- 2.7x: assets/blender/2.79/quad2tricross.zip
- 2.80: assets/blender/2.80/quad2tricross.zip

## demo project

* https://gitlab.com/frankiezafe/SoftSkin-demo

## remotes

* https://gitlab.com/frankiezafe/SoftSkin - origin
* https://github.com/frankiezafe/SoftSkin - mirror
* https://bitbucket.org/frankiezafe/softskin - hg legacy repo, [OF](http://openframeworks.cc) version

## compilation

This addon requires a recompilation of the engine.

* clone it in godot *modules* and rename the folder *softskin*.
* open a terminal and recompile godot using `scons platform=[x11,osx,windows]`.

Check the [doc](https://docs.godotengine.org/en/3.1/development/compiling/index.html) for more details.

If you want to install the icons, just run `python install.py` in the softsin folder.

## notes

### 17/10/2019

branch master is now available (proxy > xp > master)

### 06/08/2018

this repo has been started on commit 92f1deb

revert current repo to this revision to compile it

 git reset --hard 92f1deb 

### 16/05/2018

Recreation of meshes at each update is FAR too slow for soft skin. The method was inspired by csg shapes. See *Skin::bind_root()* method.

Softbodies are very close to softskin in their updates: all vertices has to be refreshed at each frame.

**SoftBodyVisualServerHandler** class (scene/3d/softbody.h) is a life-saver: it uses a call to *mesh_surface_update_region* in method *commit_changes*, wich is certainly the most efficient way to push new positions in a mesh!

```c++
void SoftBodyVisualServerHandler::commit_changes() {
	VS::get_singleton()->mesh_surface_update_region(mesh, surface, 0, buffer);
}
```

It might be a bit more complex to create the arrays, but it is worth trying!

### 11/07/2018

Integration in godot is becoming stable:

* Skin object can be linked to a server to manage them by group
* A custom notification system enables reparenting of servers and skins once placed in the scene tree
* Ray projection are working well

Still to be done:

* importation of uvs (mandatory!)
* collisions with solid parallelipipeds
* creation of fibers group:
* * requires an addon in blender
* * adaptation of the export format
* * specific methods to interacts with groups
* breakeable fibers
* splittable skins, along seams

### 06/06/2018

after serveral hours of engine hell:

* investigate CSG module > Immediate geometries have no shadows and Mesh can not be updated...
* transform Skin object into Geometries manager -> enabling several meshes (debug views, wireframe + tri mesh)

### 03/06/2018

* NOPE - ~~main object should inherits [GeometryInstance](http://docs.godotengine.org/en/3.0/classes/class_geometryinstance.html#class-geometryinstance) instead of [ImmediateGeometry](http://docs.godotengine.org/en/3.0/classes/class_immediategeometry.html#class-immediategeometry) with a fixed memory allocation - data managed by SkinDot especially should be pointers on arrays of floats, in a glBufferData for instance (if working) -> saving process and memory~~
* using glBufferData for better performance https://stackoverflow.com/questions/14155615/opengl-updating-vertex-buffer-with-glbufferdata#14156027
* glBufferData for Opengl ES3: https://www.khronos.org/registry/OpenGL-Refpages/es3.0/html/glBufferData.xhtml
* maybe switch a part of the process to C, no real need of c++ here, if std vector3 methods like distance and normalized are easy to implement efficiently
