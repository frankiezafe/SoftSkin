/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_proxy.cpp
 * Author: frankiezafe
 * 
 * Created on July 15, 2019, 6:04 PM
 */

#include "softskin_proxy.h"

SoftskinProxy::SoftskinProxy() {
}

SoftskinProxy::~SoftskinProxy() {
}

Ref<Mesh> SoftskinProxy::get_mesh() {
    return _mesh;
}

void SoftskinProxy::set_mesh(Ref<Mesh> m) {

    std::cout << "set_mesh" << std::endl;
    
    if (!_mesh.is_null()) {
        _mesh->disconnect("changed", this, "mesh_changed");
    }

    // removing previous cache & mesh if a valid skin is loaded
    if (m.is_valid()) {
        if (_cache.is_valid()) {
            set_cache(Ref<SoftskinCache>());
        }
        if (_skin.is_valid()) {
            set_skin(Ref<SoftskinMesh>());
        }
    }

    _mesh = m;
    mesh_changed();

    if (!_mesh.is_null()) {
        _mesh->connect("changed", this, "mesh_changed");
    }
    
}

Ref<SoftskinCache> SoftskinProxy::get_cache() {
    return _cache;
}

void SoftskinProxy::set_cache(Ref<SoftskinCache> m) {

    if (!_cache.is_null()) {
        _cache->disconnect("changed", this, "cache_changed");
    }

    _cache = m;
    cache_changed();

    if (!_cache.is_null()) {
        _cache->connect("changed", this, "cache_changed");
    }

}

Ref<SoftskinMesh> SoftskinProxy::get_skin() {
    return _skin;
}

void SoftskinProxy::set_skin(Ref<SoftskinMesh> m) {

    if (!_skin.is_null()) {
        _skin->disconnect("changed", this, "skin_changed");
    }

    // removing previous cache & mesh if a valid skin is loaded
    if (m.is_valid()) {
        if (_cache.is_valid()) {
            set_cache(Ref<SoftskinCache>());
        }
        if (_mesh.is_valid()) {
            set_mesh(Ref<Mesh>());
        }
    }

    _skin = m;
    skin_changed();

    if (!_skin.is_null()) {
        _skin->connect("changed", this, "skin_changed");
    }

}

bool SoftskinProxy::analyse_mesh() {

    if (_mesh.is_null()) {
        return false;
    }

    SoftskinCache* tmp = memnew(SoftskinCache);
    tmp->load(_mesh);

    if (!_cache.is_null() && _cache->compare(tmp)) {
        memdelete(tmp);
        return !_cache->is_processed();
    }
    
    _cache = Ref<SoftskinCache>(tmp);

    return true;

}

int SoftskinProxy::vector3_find(const Vector3& p, int surfid) {
    for (int i = 0, imax = _cache->_unique_dots.size(); i < imax; ++i) {
        Vector3 d = _cache->_surfaces[surfid]->verts[ _cache->_unique_dots[i].mirrors[0] ] - p;
        if (d.length_squared() < VERTICE_MERGE_DISTANCE) {
            return i;
        }
    }
    return -1;
}

void SoftskinProxy::analyse_cache() {

    if (_mesh.is_null() || _cache.is_null()) {
        std::cout << "_mesh or _cache is null!" << std::endl;
        return;
    }

    if (_cache->_surfaces.size() == 0) {
        std::cout << "NO SURFACE in _cache!" << std::endl;
        return;
    }

    _cache->purge_analysis();

    // parsing surface 0 ONLY
    uint32_t SURF_ID = 0;

    // retrieval of mesh vertices and detection of uniques
    PoolVector<int>& indices = _cache->_surfaces[SURF_ID]->indices;
    PoolVector<Vector3>& _all_vertices = _cache->_surfaces[SURF_ID]->verts;

    // generation of unique vertices
    for (int i = 0, imax = _all_vertices.size(); i < imax; ++i) {
        int pos = vector3_find(_all_vertices[i], SURF_ID);
        if (pos == -1) {
            v3_ref vf;
            vf.mirrors.push_back(i);
            vf = _all_vertices[i];
            pos = _cache->_unique_dots.size();
            _cache->_unique_dots.push_back(vf);
        } else {
            _cache->_unique_dots[pos].mirrors.push_back(i);
        }
        _cache->_all2unique[i] = pos;
    }

    _cache->_unique_normals.resize(_all_vertices.size());

    // generation of unique edges
    int facei = 0;
    _cache->_face_all_verts[facei].resize(3);
    bool dirty_mesh = false;
    for (int i = 0, imax = indices.size(); i < imax; ++i) {

        int index = indices[i];
        _cache->_face_all_verts[facei][i % 3] = index;
        int index_next = indices[(i / 3)*3 + (i + 1) % 3]; // next

        // adding face index in unique normal
        _cache->_unique_normals[index].push_face(facei);

        // getting unique vertex indices
        int uindex = _cache->_all2unique[index];
        int nextuindex = _cache->_all2unique[index_next];
        v3_ref& vr = _cache->_unique_dots[uindex];
        vr.belong_2_face = true;
        edge_ref er;

        if (er.set(uindex, nextuindex)) {
            int eindex = 0;
            std::vector< edge_ref >::iterator ei = _cache->_unique_fibers.begin();
            std::vector< edge_ref >::iterator ee = _cache->_unique_fibers.end();
            for (; ei != ee; ++ei, ++eindex) {
                if ((*ei) == er) {
                    break;
                }
            }
            if (ei == ee) {
                er.ligament = false;
                er.faces.push_back(facei);
                _cache->_unique_fibers.push_back(er);
            } else {
                (*ei).push_face(facei);
            }
            if (std::find(
                    _cache->_face_fibers[facei].begin(),
                    _cache->_face_fibers[facei].end(),
                    eindex
                    ) == _cache->_face_fibers[facei].end()
                    ) {
                _cache->_face_fibers[facei].push_back(eindex);
            }
        } else {
            dirty_mesh = true;
        }

        if (i > 0 && i % 3 == 2) {
            ++facei;
            _cache->_face_all_verts[facei].resize(3);
        }

    }

    // generation of unique normals
    std::vector<normal_ref>::iterator ni;
    ni = _cache->_unique_normals.begin();
    std::vector<normal_ref>::iterator ne = _cache->_unique_normals.end();
    uint32_t nindex = 0;

    for (; ni != ne; ++ni, ++nindex) {

        int uindex = _cache->_all2unique[nindex];
        normal_ref& nr = (*ni);
        nr.dot_id = uindex;
        int imax = nr.faces.size();
        nr.edges.resize(imax);

        for (int i = 0; i < imax; ++i) {
            ss_unique_vec& eindices = _cache->_face_fibers[nr.faces[i]];
            two_edge te;
            te.buffer_id = nindex;
            for (int j = 0, jmax = eindices.size(); j < jmax; ++j) {
                edge_ref& ef = _cache->_unique_fibers[ eindices[j] ];
                int contain = ef.contains(uindex);
                if (contain != -1) {
                    if (te.edge0 == UINT_MAX) {
                        te.edge0 = eindices[j];
                        te.flip0 = contain == 1;
                    } else {
                        te.edge1 = eindices[j];
                        te.flip1 = contain == 1;
                        break;
                    }
                }
            }
            if (te.edge0 != UINT_MAX && te.edge1 != UINT_MAX) {
                // swapping the edge to match CULL_BACK
                ss_unique_vec& indices = _cache->_face_all_verts[nr.faces[i]];
                // where is the current index in the list?
                int p = 0;
                if (indices[1] == nindex) {
                    p = 1;
                } else if (indices[2] == nindex) {
                    p = 2;
                }
                // previous edge is : (p+2)%3
                // next edge is : (p+1)%3
                // with that, we can check the edge0
                // getting the v3_ref of the tail:
                int v3id0 = _cache->_unique_fibers[te.edge0].v3_ref_1;
                if (te.flip0) {
                    v3id0 = _cache->_unique_fibers[te.edge0].v3_ref_0;
                }
                //                int v3id1 = _unique_edges[te.edge1].v3_ref_1;
                //                if (te.flip1) {
                //                    v3id1 = _unique_edges[te.edge1].v3_ref_0;
                //                }
                if (_cache->_unique_dots[v3id0].find_mirror(indices[ (p + 2) % 3 ]) == -1) {
                    te.swap();
                }
                nr.edges[i] = te;
            }

        }
    }

    if (dirty_mesh) {
        std::cout << "SoftskinProxy::prepare_mesh, you mesh is dirty! "
                "some of the faces have a surface 0 "
                "(at least 2 vertices at the same position)" <<
                std::endl;
    }

    cache_changed();

}

void SoftskinProxy::mesh_changed() {

    if (!_mesh.is_null()) {
        if (analyse_mesh()) {
            if (!_cache->is_processed()) {
                analyse_cache();
            }
        }
    }

}

void SoftskinProxy::cache_changed() {

    if (!_cache.is_null() && _cache->is_processed()) {
        Resource::emit_changed();
    } else if (_cache.is_null()) {
        Resource::emit_changed();
    }

}

/* Turning skin_mesh into skin_cache, ready to be loqded by skin*/
void SoftskinProxy::skin_changed() {

    if (_skin.is_valid() && _skin->get_cache().is_valid()) {
        set_cache(_skin->get_cache());
    }

}

void SoftskinProxy::_bind_methods() {

    // used by signal
    ClassDB::bind_method(D_METHOD("mesh_changed"), &SoftskinProxy::mesh_changed);
    ClassDB::bind_method(D_METHOD("cache_changed"), &SoftskinProxy::cache_changed);
    ClassDB::bind_method(D_METHOD("skin_changed"), &SoftskinProxy::cache_changed);

    ClassDB::bind_method(D_METHOD("set_cache", "cache"), &SoftskinProxy::set_cache);
    ClassDB::bind_method(D_METHOD("get_cache"), &SoftskinProxy::get_cache);

    ClassDB::bind_method(D_METHOD("set_mesh", "mesh"), &SoftskinProxy::set_mesh);
    ClassDB::bind_method(D_METHOD("get_mesh"), &SoftskinProxy::get_mesh);

    ClassDB::bind_method(D_METHOD("set_skin", "skin"), &SoftskinProxy::set_skin);
    ClassDB::bind_method(D_METHOD("get_skin"), &SoftskinProxy::get_skin);

    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "cache", PROPERTY_HINT_RESOURCE_TYPE, "SoftskinCache"), "set_cache", "get_cache");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "skin", PROPERTY_HINT_RESOURCE_TYPE, "SoftskinMesh"), "set_skin", "get_skin");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "mesh", PROPERTY_HINT_RESOURCE_TYPE, "Mesh"), "set_mesh", "get_mesh");

}

void SoftskinProxy::_notification(int p_what) {
}