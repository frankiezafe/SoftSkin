/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_io.cpp
 * Author: frankiezafe
 * 
 * Created on July 16, 2019, 3:02 PM
 */

#include "softskin_io_mesh.h"

// SAVER

ResourceFormatSaverSoftskinMesh::ResourceFormatSaverSoftskinMesh() {
}

Error ResourceFormatSaverSoftskinMesh::save(const String &p_path, const RES &p_resource, uint32_t p_flags) {
    const SoftskinMesh* sc = Object::cast_to<SoftskinMesh>(p_resource.ptr());
    if (!sc) {
        return FAILED;
    }
//    SkinMesh* sch = (SkinMesh*) p_resource.ptr();
//    return sch->save_file(p_path);
    return OK;
}

bool ResourceFormatSaverSoftskinMesh::recognize(const RES & p_resource) const {
    return Object::cast_to<SoftskinMesh>(p_resource.ptr()) != 0;
}

void ResourceFormatSaverSoftskinMesh::get_recognized_extensions(const RES &p_resource, List<String> *p_extensions) const {
    const SoftskinMesh* sc = Object::cast_to<SoftskinMesh>(p_resource.ptr());
    if (sc) {
        p_extensions->clear();
        p_extensions->push_back(SSM_EXT);
    }

}

// LOADER

ResourceFormatLoaderSoftskinMesh::ResourceFormatLoaderSoftskinMesh() {
}

RES ResourceFormatLoaderSoftskinMesh::load(const String &p_path, const String &p_original_path, Error * r_error) {
    SoftskinMesh *sc = memnew(SoftskinMesh);
    if (r_error) {
        *r_error = OK;
    }
    Error err = sc->load_file(p_path);
    if (err != OK) {
        return Ref<SoftskinMesh>();
    }
    Ref<SoftskinMesh> ss_mesh = Ref<SoftskinMesh>(sc);
    return ss_mesh;
}

void ResourceFormatLoaderSoftskinMesh::get_recognized_extensions(List<String> *p_extensions) const {
    p_extensions->push_back(SSM_EXT);
}

bool ResourceFormatLoaderSoftskinMesh::handles_type(const String & p_type) const {
    return ClassDB::is_parent_class(p_type, SSM_NAME);
}

String ResourceFormatLoaderSoftskinMesh::get_resource_type(const String & p_path) const {
    if (p_path.get_extension().to_lower() == SSM_EXT) {
        return SSM_NAME;
    }
    return "";
}