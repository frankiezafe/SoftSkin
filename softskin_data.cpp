/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_data.cpp
 * Author: frankiezafe
 * 
 * Created on July 15, 2019, 6:55 PM
 */

#include "softskin_data.h"

v3_ref::v3_ref() :
belong_2_face(false),
local_id(UINT_MAX) {
}

int v3_ref::find_mirror(uint32_t i) const {
    return mirrors.find(i);
}

two_edge::two_edge() :
buffer_id(UINT_MAX),
edge0(UINT_MAX),
edge1(UINT_MAX),
flip0(false),
flip1(false) {
}

void two_edge::swap() {
    uint32_t tmpi = edge0;
    bool tmpb = flip0;
    edge0 = edge1;
    flip0 = flip1;
    edge1 = tmpi;
    flip1 = tmpb;
}

normal_ref::normal_ref() :
dot_id(-1) {
}

void normal_ref::push_face(uint32_t i) {
    if (faces.find(i) == -1) {
        faces.push_back(i);
    }
}

edge_ref::edge_ref() :
v3_ref_0(UINT_MAX),
v3_ref_1(UINT_MAX),
ligament(false) {
}

bool edge_ref::set(uint32_t v0, uint32_t v1) {
    if (v0 < v1) {
        v3_ref_0 = v0;
        v3_ref_1 = v1;
    } else if (v0 > v1) {
        v3_ref_0 = v1;
        v3_ref_1 = v0;
    } else {
        return false;
    }
    return true;
}

int edge_ref::contains(uint32_t vid) {
    if (vid == v3_ref_0) {
        return 0;
    } else if (vid == v3_ref_1) {
        return 1;
    } else {
        return -1;
    }
}

void edge_ref::push_face(uint32_t i) {
    if (faces.find(i) == -1) {
        faces.push_back(i);
    }
}

SoftskinData::SoftskinData() :
surface_format(0),
primitive_type(0),
stride(0),
offset_vertices(0),
offset_normal(0),
offset_uv(0),
geom_buffer_num(0),
verts_num(0),
normals_num(0),
forces_num(0),
dots_num(0),
anchors_num(0),
fibers_num(0),
ligaments_num(0),
groups_num(0),
vert_buffer(0),
norm_buffer(0),
verts(0),
forces(0),
dots(0),
anchors(0),
fibers(0),
ligaments(0),
normals(0),
groups(0),
_valid(false) {
}

SoftskinData::~SoftskinData() {
    purge();
}

void SoftskinData::purge_anchors() {

    purge_ligaments();

    if (anchors) {
        memdelete_arr(anchors);
    }

    anchors_num = 0;
    anchors = 0;

}

void SoftskinData::purge_ligaments() {

    if (groups) {
        for (uint32_t g = 0; g < groups_num; ++g) {
            const softskin_type_t& t = groups[g]->get_type();
            switch (t) {
                case sf_EDGE:
                {
                    SoftskinGroupEdge* sge = (SoftskinGroupEdge*) groups[g];
                    for (uint32_t i = 0; i < ligaments_num; ++i) {
                        sge->erase(&ligaments[i]);
                    }
                }
                    break;
                case sf_LIGAMENT:
                case sf_MUSCLE:
                {
                    SoftskinGroupLigament* sgl = (SoftskinGroupLigament*) groups[g];
                    for (uint32_t i = 0; i < ligaments_num; ++i) {
                        sgl->erase(&ligaments[i]);
                    }
                }
                    break;
                default:
                    break;
            }
        }
    }

    if (ligaments) {
        memdelete_arr(ligaments);
    }
    ligaments_num = 0;
    ligaments = 0;

}

void SoftskinData::purge() {

    if (vert_buffer) {
        memdelete_arr(vert_buffer);
    }
    if (norm_buffer) {
        memdelete_arr(norm_buffer);
    }
    if (verts) {
        memdelete_arr(verts);
    }
    if (dots) {
        memdelete_arr(dots);
    }
    if (fibers) {
        memdelete_arr(fibers);
    }
    if (normals) {
        memdelete_arr(normals);
    }
    if (groups) {
        for (uint32_t i = 0; i < groups_num; ++i) {
            delete groups[i];
        }
        memdelete_arr(groups);
    }

    vert_buffer = 0;
    norm_buffer = 0;
    verts = 0;
    forces = 0;
    dots = 0;
    fibers = 0;
    normals = 0;
    groups = 0;

    geom_buffer_num = 0;
    verts_num = 0;
    normals_num = 0;
    forces_num = 0;
    dots_num = 0;
    fibers_num = 0;
    groups_num = 0;

    purge_anchors();

    _valid = false;

}

void SoftskinData::validate() {
    _valid =
            (vert_buffer != 0) &&
            (verts != 0) &&
            (dots != 0) &&
            (fibers != 0) &&
            (normals != 0);
}

const bool& SoftskinData::is_valid() const {
    return _valid;
}