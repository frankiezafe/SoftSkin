/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 *  
 *  This file is part of softskin library
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2018 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

#include "register_types.h"

#include "softskin_cache.h"
#include "softskin_mesh.h"
#include "softskin_group.h"
#include "softskin_proxy.h"
#include "softskin.h"

#include "softskin_io_cache.h"
#include "softskin_io_mesh.h"

static Ref<ResourceFormatSaverSoftskinCache> softskin_cache_saver;
static Ref<ResourceFormatLoaderSoftskinCache> softskin_cache_loader;
static Ref<ResourceFormatSaverSoftskinMesh> softskin_mesh_saver;
static Ref<ResourceFormatLoaderSoftskinMesh> softskin_mesh_loader;

#ifdef TOOLS_ENABLED

static void _editor_init() {
    //	Ref<EditorSceneImporterAssimp> import_assimp;
    //	import_assimp.instance();
    //	ResourceImporterScene::get_singleton()->add_importer(import_assimp);
    std::cout << "softskin >> _editor_init" << std::endl;
}
#endif

void register_softskin_types() {

    ClassDB::register_class<SoftskinCache>();
    ClassDB::register_class<SoftskinMesh>();
    ClassDB::register_class<SoftskinProxy>();
    ClassDB::register_class<SoftskinDebug>();
    ClassDB::register_class<Softskin>();

    ClassDB::register_class<SoftskinGroupDot>();
    ClassDB::register_class<SoftskinGroupFiber>();
    ClassDB::register_class<SoftskinGroupLigament>();

    softskin_cache_saver.instance();
    ResourceSaver::add_resource_format_saver(softskin_cache_saver);
    softskin_cache_loader.instance();
    ResourceLoader::add_resource_format_loader(softskin_cache_loader);
    softskin_mesh_saver.instance();
    ResourceSaver::add_resource_format_saver(softskin_mesh_saver);
    softskin_mesh_loader.instance();
    ResourceLoader::add_resource_format_loader(softskin_mesh_loader);

#ifdef TOOLS_ENABLED
    //	ClassDB::APIType prev_api = ClassDB::get_current_api();
    //	ClassDB::set_current_api(ClassDB::API_EDITOR);
    //	ClassDB::register_class<EditorSceneImporterAssimp>();
    //	ClassDB::set_current_api(prev_api);
    //	EditorNode::add_init_callback(_editor_init);
#endif
    //    softskin_cache_saver.instance();
    //    ResourceSaver::add_resource_format_saver(softskin_cache_saver);
    //    softskin_cache_loader = memnew(ResourceFormatLoaderSoftskinCache);
    //    ResourceLoader::add_resource_format_loader(softskin_cache_loader);
    //    softskin_mesh_saver.instance();
    //    ResourceSaver::add_resource_format_saver(softskin_mesh_saver);
    //    softskin_mesh_loader = memnew(ResourceFormatLoaderSoftskinMesh);
    //    ResourceLoader::add_resource_format_loader(softskin_mesh_loader);

}

void unregister_softskin_types() {

    ResourceSaver::remove_resource_format_saver(softskin_cache_saver);
    softskin_cache_saver.unref();
    ResourceLoader::remove_resource_format_loader(softskin_cache_loader);
    softskin_cache_loader.unref();
    ResourceSaver::remove_resource_format_saver(softskin_mesh_saver);
    softskin_mesh_saver.unref();
    ResourceLoader::remove_resource_format_loader(softskin_mesh_loader);
    softskin_mesh_loader.unref();

    //    ResourceSaver::remove_resource_format_saver(softskin_cache_saver);
    //    softskin_cache_saver.unref();
    //    memdelete(softskin_cache_loader);
    //    ResourceSaver::remove_resource_format_saver(softskin_mesh_saver);
    //    softskin_cache_saver.unref();
    //    memdelete(softskin_mesh_loader);

}
