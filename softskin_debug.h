/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_debug.h
 * Author: frankiezafe
 *
 * Created on July 18, 2019, 6:44 PM
 */

#ifndef SOFTSKIN_DEBUG_H
#define SOFTSKIN_DEBUG_H

#include "scene/3d/visual_instance.h"
#include "softskin_common.h"

class SoftskinDebug : public GeometryInstance {
    GDCLASS(SoftskinDebug, GeometryInstance);

public:
    
    SoftskinDebug();
    ~SoftskinDebug();
    
    Ref<Material> get_material();
    void set_material( Ref<Material> );
    
    void albedo( const Color& c );
    void vertex_color( const bool& enable );
    void unshaded( const bool& enable );
    void line_width( const float& w );
    
    void start();
    void push( const Vector3& v0, const Vector3& v1, const Color* c0 = 0, const Color* c1 = 0 );
    void end();

    virtual AABB get_aabb() const { return aabb; }
    virtual PoolVector<Face3> get_faces(uint32_t p_usage_flags) const { return _faces; }

protected:
    
    AABB aabb;
    PoolVector<Face3> _faces;
    
    RID _imm;
    Ref<SpatialMaterial> _mat;
    bool _own_mat;
    
    void generate_material();
    
};

#endif /* SOFTSKIN_DEBUG_H */

