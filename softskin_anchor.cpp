/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skinanchor.cpp
 * Author: frankiezafe
 * 
 * Created on May 6, 2019, 2:04 PM
 */

#include "softskin_anchor.h"

SkinAnchor::SkinAnchor() :
_skin_root(0),
_inititalised(false) {
}

SkinAnchor::~SkinAnchor() {
    purge();
}

void SkinAnchor::purge() {
    
    _skin_root = 0;
    _inititalised = false;
//    mirror_anchors.purge();
    
}

void SkinAnchor::init(const Vector3& v3, Spatial* skin_root) {
    
    ERR_FAIL_COND(_inititalised);
    
    _skin_root = skin_root;
    _parent = skin_root;
    _anchor.init(v3);
    init_internal();
    
}

void SkinAnchor::init(const float& x, const float& y, const float& z, Spatial* skin_root) {
    
    ERR_FAIL_COND(_inititalised);
    
    _skin_root = skin_root;
    _parent = skin_root;
    _anchor.init(Vector3(x, y, z));
    init_internal();
    
}

void SkinAnchor::init_internal() {
    
    _anchor_origin = _anchor.ref();
//    mirror_anchors.src = &_anchor;
    _inititalised = true;
    
}

void SkinAnchor::register_anchor(Vector3* anchor) {
    
    ERR_FAIL_COND(!_inititalised);
//    mirror_anchors.add(anchor);
    
}

const VectorPtr< Vector3 >& SkinAnchor::anchor() const {
    return _anchor;
}

void SkinAnchor::anchor(const float& x, const float& y, const float& z) {
    _anchor = Vector3(x, y, z);
}

void SkinAnchor::parent( Spatial* p ) {
    
    ERR_FAIL_COND(!_inititalised);
    
    if ( p != _parent ) {
        
        Vector3 v = _parent->get_global_transform().xform(_anchor_origin);
        if ( !p ) {
            p = _skin_root;
        }
        _anchor_origin = p->to_local(v);
        _parent = p;
        
    }
    
}

const Vector3& SkinAnchor::update(const float& delta_time) {

    if (!_inititalised) {
        return _anchor_origin;
    }

    if (_parent != _skin_root) {
        Vector3 v = _parent->get_global_transform().xform(_anchor_origin);
        _anchor = _skin_root->to_local(v);
    }

//    mirror_anchors.sync();
    return _anchor.ref();
    
}