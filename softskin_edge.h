/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SkinEdge.h
 * Author: frankiezafe
 *
 * Created on May 14, 2019, 8:28 AM
 */

#ifndef SOFTSKINEDGE_H
#define SOFTSKINEDGE_H

#include <iostream>
#include <ostream>
#include <vector>
#include "core/math/vector3.h"

#include "softskin_dot.h"
#include "softskin_anchor.h"

// base class of all fibers, tensors, ligaments and muscles,
// allows to manipulate them blindly

class SoftskinEdge {
public:

    //***********
    //* STATICS *
    //***********

    static std::string print_type(const softskin_type_t& t);

    SoftskinEdge();

    //***********
    //* GETTERS *
    //***********

    const softskin_type_t& get_type() const;

    const Vector3& get_dir() const;

    const Vector3& get_middle() const;

    const float& get_current_len() const;

    const float& get_rest_len() const;

    const float& get_rest_len_multiplier() const;

    const float& get_init_rest_len() const;

    const float& get_stiffness() const;

    const float& get_muscle_min() const;

    const float& get_muscle_max() const;

    const bool& get_muscle() const;

    const float& get_muscle_frequency() const;

    //***********
    //* SETTERS *
    //***********

    void set_len(const float& l);

    void set_rest_len(const float& l);

    void set_rest_len_multiplier(const float& l);

    void set_stiffness(const float& s);

    void set_muscle_min_max(float min, float max);

    void set_muscle_min(float min);

    void set_muscle_max(float max);

    void set_muscle_frequency(float f);

    void set_muscle_phase_shift(float shift);

    bool muscle(bool enable);

    bool musclise(float min, float max, float freq, float shift);

    virtual const Vector3& update(const float& delta_time);

protected:

    softskin_type_t _type;

    Vector3 _dir;
    Vector3 _middle;
    float _current_len;
    float _rest_len;
    float _rest_len_multiplier;
    float _init_rest_len;

    float _stiffness;
    Vector3 _force_feedback;

    bool _muscled;
    float _muscle_min_len_requested;    
    float _muscle_min_len;
    float _muscle_max_len;
    float _muscle_delta_len;
    float _muscle_a;
    float _muscle_phase_shift;
    float _muscle_frequency;

    void update_muscle(float delta_time);

    virtual void update_specialised() = 0;

};

template< class A, class B >
class SkinEdgeAdvanced : public SoftskinEdge {
public:

    SkinEdgeAdvanced() :
    _head(0),
    _tail(0) {
    }

    //**************
    //* PROCESSING *
    //**************

    virtual bool init(A* ptr0, B* ptr1) {
        std::cout <<
                "SoftskinEdgeAdvanced::init, "
                "no specialisation the template for this kind of objects!!" <<
                std::endl;
        return false;
    }

    //***********
    //* GETTERS *
    //***********

    const A* get_head() {
        return _head;
    }

    const B* get_tail() {
        return _tail;
    }

protected:

    A* _head;
    B* _tail;

};

#endif /* SOFTSKINEDGE_H */

