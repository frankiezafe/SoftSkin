/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin.h
 * Author: frankiezafe
 *
 * Created on July 15, 2019, 6:12 PM
 */

#ifndef SOFTSKIN_H
#define SOFTSKIN_H

#include "scene/3d/visual_instance.h"

#include "softskin_common.h"
#include "softskin_proxy.h"
#include "softskin_debug.h"

class Softskin : public VisualInstance {
    GDCLASS(Softskin, VisualInstance);
    OBJ_CATEGORY("Softskin");

public:

    Softskin();
    virtual ~Softskin();

    Ref<SoftskinProxy> get_proxy();
    void set_proxy(Ref<SoftskinProxy>);

    Ref<Material> get_material();
    void set_material(Ref<Material>);

    Ref<Material> get_ligaments_material();
    void set_ligaments_material(Ref<Material>);

    Ref<Material> get_normal_material();
    void set_normal_material(Ref<Material>);

    Ref<Material> get_wireframe_material();
    void set_wireframe_material(Ref<Material>);

    const bool& get_generate_ligaments() const;
    void set_generate_ligaments(const bool& b);

    const bool& get_display_skin() const;
    void set_display_skin(const bool& b);

    bool get_display_ligaments() const;
    void set_display_ligaments(const bool& b);

    bool get_display_normals() const;
    void set_display_normals(const bool& b);

    bool get_display_edges() const;
    void set_display_edges(const bool& b);

    const float& get_normal_length() const;
    void set_normal_length(const float& b);

    const float& get_line_width() const;
    void set_line_width(const float& b);

    const Vector3& get_gravity() const;
    void set_gravity(const Vector3& g);

    SoftskinGroupDot* get_dots(const String& name);
    SoftskinGroupFiber* get_fibers(const String& name);
    SoftskinGroupLigament* get_ligaments(const String& name);
    
    Vector<String> get_dot_groups();
    Vector<String> get_fiber_groups();
    Vector<String> get_ligament_groups();

    Vector3 get_global_barycenter(const String& name);
    Vector3 get_local_barycenter(const String& name);

    AABB get_aabb() const;
    PoolVector<Face3> get_faces(uint32_t p_usage_flags) const;

protected:

    bool _set(const StringName &p_name, const Variant &p_value);
    bool _get(const StringName &p_name, Variant &r_ret) const;
    void _get_property_list(List<PropertyInfo> *p_list) const;

    static void _bind_methods();

    void _validate_property(PropertyInfo &property) const;

    void _notification(int p_what);

    void proxy_changed();

private:

    AABB _bbox;
    PoolVector<Face3> _faces;

    RID _mesh;
    SoftskinDebug* _debug_ligaments;
    SoftskinDebug* _debug_normals;
    SoftskinDebug* _debug_egdes;
    Ref<SoftskinProxy> _proxy;
    Ref<Material> _mat;
    Ref<Material> _material_ligaments;
    Ref<Material> _material_normals;
    Ref<Material> _material_wireframe;

    SoftskinData _data;
    PoolVector<uint8_t> _buffer;
    PoolVector<uint8_t>::Write _write_buffer;

    bool _generate_ligaments;
    bool _display_skin;
    float _normal_length;
    float _line_width;
    Vector3 _gravity;

    bool _enable_generate_ligaments;

    void purge();
    void generate_skinmesh();
    void generate_default_groups();
    void update_internal(const float& delta_time);
    void commit_changes();

    SoftskinGroup* get_group(const String& name, const softskin_type_t& t = sf_UNDEFINED) const;
    Vector<String> get_group_names( const softskin_type_t& t );

};

#endif /* SOFTSKIN_H */