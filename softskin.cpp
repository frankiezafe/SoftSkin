/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin.cpp
 * Author: frankiezafe
 * 
 * Created on July 15, 2019, 6:12 PM
 */

#include "softskin.h"

Softskin::Softskin() :
_debug_ligaments(0),
_debug_normals(0),
_debug_egdes(0),
_generate_ligaments(false),
_display_skin(true),
_normal_length(SS_DEBUG_NORMAL_LENGTH),
_line_width(SS_DEBUG_LINE_WIDTH),
_enable_generate_ligaments(true) {

}

Softskin::~Softskin() {
}

Ref<SoftskinProxy> Softskin::get_proxy() {
    return _proxy;
}

void Softskin::set_proxy(Ref<SoftskinProxy> p) {

    if (!_proxy.is_null()) {
        _proxy->disconnect("changed", this, "proxy_changed");
    }

    _proxy = p;
    proxy_changed();

    if (!_proxy.is_null()) {
        _proxy->connect("changed", this, "proxy_changed");
    }

}

void Softskin::proxy_changed() {

    if (
            _proxy.is_valid() &&
            _proxy->is_ready()
            ) {

        if (_mesh.is_valid()) {
            VS::get_singleton()->mesh_clear(_mesh);
        }

        _mesh = VS::get_singleton()->mesh_create();

        uint32_t SURF_ID = 0;
        SoftskinCache::MeshArray* ma = _proxy->get_cache()->_surfaces[SURF_ID];
        VS::get_singleton()->mesh_add_surface_from_arrays(_mesh, VS::PrimitiveType(ma->primitive_type), ma->arr);
        generate_skinmesh();

        if (_mat.is_valid()) {
            VS::get_singleton()->mesh_surface_set_material(_mesh, 0, _mat->get_rid());
        }
        if (_display_skin) {
            set_base(_mesh);
        }

    } else if (_mesh.is_valid()) {

        purge();

    }

}

void Softskin::update_internal(const float& delta_time) {

    if (!is_visible()) return;

    if (!_data.is_valid()) {
        return;
    }

    Vector3 partg = _gravity * delta_time;

    for (uint32_t i = 0; i < _data.dots_num; ++i) {
        _data.dots[i].push(partg);
        _data.dots[i].update(delta_time);
    }

    for (uint32_t i = 0; i < _data.anchors_num; ++i) {
        _data.anchors[i].update(delta_time);
    }

    for (uint32_t i = 0; i < _data.fibers_num; ++i) {
        _data.fibers[i].update(delta_time);
    }

    for (uint32_t i = 0; i < _data.ligaments_num; ++i) {
        _data.ligaments[i].update(delta_time);
    }

    for (uint32_t i = 0; i < _data.normals_num; ++i) {
        _data.normals[i].update();
    }

    if (_debug_ligaments != 0) {
        Color head(1.0, 0.0, 1.0);
        Color tail(0.0, 1.0, 1.0);
        _debug_ligaments->start();
        for (uint32_t i = 0; i < _data.ligaments_num; ++i) {
            SoftskinLigament& sl = _data.ligaments[i];
            _debug_ligaments->push(
                    sl.get_head()->anchor().ref(),
                    sl.get_tail()->vert().ref(),
                    &head, &tail);
        }
        _debug_ligaments->end();
    }

    if (_debug_normals != 0) {
        Color head(1.0, 0.0, 1.0);
        Color tail(0.0, 1.0, 1.0);
        _debug_normals->start();
        for (uint32_t i = 0; i < _data.normals_num; ++i) {
            Vector3 n = _data.normals[i].get_normal() * _normal_length;
            for (int f = 0, fnum = _data.normals[i].get_face_size(); f < fnum; ++f) {
                Vector3 v = _data.normals[i].dot(f)->vert().ref();
                _debug_normals->push(v, v + n, &head, &tail);
            }
        }
        _debug_normals->end();
    }

    if (_debug_egdes != 0) {
        _debug_egdes->start();
        for (uint32_t i = 0; i < _data.fibers_num; ++i) {
            SoftskinFiber& sf = _data.fibers[i];
            _debug_egdes->push(sf.get_head()->vert().ref(), sf.get_tail()->vert().ref());
        }
        _debug_egdes->end();
    }

}

void Softskin::commit_changes() {


    if (!is_visible() || !_display_skin) return;

    if (!_mesh.is_valid() || !_data.is_valid()) {
        return;
    }

    for (uint32_t i = 0; i < _data.geom_buffer_num; ++i) {
        copymem(&_write_buffer[i * _data.stride + _data.offset_vertices], _data.vert_buffer[i], sizeof (float) * 3);
    }

    if (_data.surface_format & VS::ARRAY_COMPRESS_NORMAL) {
        for (uint32_t i = 0; i < _data.geom_buffer_num; ++i) {
            int8_t vector[4] = {
                (int8_t) CLAMP(_data.norm_buffer[i][0] * 127, -128, 127),
                (int8_t) CLAMP(_data.norm_buffer[i][1] * 127, -128, 127),
                (int8_t) CLAMP(_data.norm_buffer[i][2] * 127, -128, 127),
                0,
            };
            copymem(&_write_buffer[i * _data.stride + _data.offset_normal], vector, 4);
        }
    } else {
        for (uint32_t i = 0; i < _data.geom_buffer_num; ++i) {
            copymem(&_write_buffer[i * _data.stride + _data.offset_normal], _data.norm_buffer[i], sizeof (float) * 3);
        }
    }

    VS::get_singleton()->mesh_surface_update_region(_mesh, 0, 0, _buffer);

    // _bbox = _mesh->get_aabb();

}

void Softskin::purge() {

    _write_buffer = PoolVector<uint8_t>::Write();
    _buffer.resize(0);
    _data.purge();
    if (_mesh.is_valid()) {
        VS::get_singleton()->mesh_clear(_mesh);
    }

    set_display_ligaments(false);
    set_display_normals(false);
    set_display_edges(false);

}

void Softskin::generate_default_groups() {

    _data.groups_num = ID_GROUP_MAX;
    if (_data.groups_num > 0) {
        _data.groups = memnew_arr(SoftskinGroup*, _data.groups_num);
    }
    for (uint32_t i = 0; i < _data.groups_num; ++i) {
        switch (i) {
            case ID_GROUP_ALL_FIBERS:
            {
                SoftskinGroupFiber* sgf = new SoftskinGroupFiber();
                sgf->set_name(NAME_GROUP_ALL_FIBERS);
                for (uint32_t f = 0; f < _data.fibers_num; ++f) {
                    if (!_data.fibers[f].get_muscle()) {
                        sgf->push_back(&_data.fibers[f]);
                    }
                }
                sgf->pack();
                _data.groups[i] = sgf;
            }
                break;
            case ID_GROUP_ALL_LIGAMENTS:
            {
                SoftskinGroupLigament* sgl = new SoftskinGroupLigament();
                sgl->set_name(NAME_GROUP_ALL_LIGAMENTS);
                for (uint32_t l = 0; l < _data.ligaments_num; ++l) {
                    if (!_data.ligaments[l].get_muscle()) {
                        sgl->push_back(&_data.ligaments[l]);
                    }
                }
                sgl->pack();
                _data.groups[i] = sgl;
            }
                break;
            case ID_GROUP_ALL_DOTS:
            {
                SoftskinGroupDot* sgd = new SoftskinGroupDot();
                sgd->set_name(NAME_GROUP_ALL_DOTS);
                for (uint32_t d = 0; d < _data.verts_num; ++d) {
                    sgd->push_back(&_data.dots[d]);
                }
                sgd->pack();
                _data.groups[i] = sgd;
            }
                break;
            default:
                break;
        }
    }

}

void Softskin::generate_skinmesh() {

    if (_proxy.is_null() || !_proxy->is_ready()) {
        return;
    }

    SoftskinCache* ca = _proxy->get_cache().ptr();

    // just to be sure
    _data.purge();

    uint32_t SURF_ID = 0;

    // copying all buffer related info
    ca->copy_bufferinfo(&_data, SURF_ID);

    // let's generate the hell of a memory    
    PoolVector<Vector3>& _all_vertices = ca->_surfaces[SURF_ID]->verts;
    PoolVector<Vector3>& _all_normals = ca->_surfaces[SURF_ID]->normals;
    ss_unique_map& _all2unique = ca->_all2unique;
    std::vector<v3_ref>& _unique_vertices = ca->_unique_dots;
    std::vector<edge_ref>& _unique_edges = ca->_unique_fibers;
    std::vector< normal_ref >& _unique_normals = ca->_unique_normals;

    // VERTICES
    _data.verts_num = _unique_vertices.size();
    if (_data.verts_num > 0) {
        _data.verts = memnew_arr(Vector3, _data.verts_num);
    }
    _data.forces_num = _data.verts_num;
    if (_data.forces_num > 0) {
        _data.forces = memnew_arr(Vector3, _data.forces_num);
    }
    _data.dots_num = _data.verts_num;
    if (_data.dots_num > 0) {
        _data.dots = memnew_arr(SoftskinDot, _data.dots_num);
    }
    for (uint32_t i = 0; i < _data.verts_num; ++i) {
        _data.verts[i] = _unique_vertices[i].xyz;
        _data.dots[i].init(&_data.verts[i], &_data.forces[i]);
    }

    // NORMALS
    _data.normals_num = _unique_normals.size();
    if (_data.normals_num > 0) {
        _data.normals = memnew_arr(SoftskinNormal, _data.normals_num);
    }

    // EDGES
    _data.fibers_num = _unique_edges.size();
    if (_data.fibers_num > 0) {
        _data.fibers = memnew_arr(SoftskinFiber, _data.fibers_num);
    }
    for (uint32_t i = 0; i < _data.fibers_num; ++i) {
        edge_ref& er = _unique_edges[i];
        _data.fibers[i].init(&_data.dots[er.v3_ref_0], &_data.dots[er.v3_ref_1]);
    }

    // MESH BUFFERS: VERTICES & NORMALS
    _data.geom_buffer_num = _all_vertices.size();
    if (_data.geom_buffer_num > 0) {
        _data.vert_buffer = memnew_arr(const real_t*, _data.geom_buffer_num);
        _data.norm_buffer = memnew_arr(const real_t*, _data.geom_buffer_num);
    }

    // linking stuff : DOTS > VERTICES
    for (uint32_t i = 0; i < _data.geom_buffer_num; ++i) {
        _data.vert_buffer[i] = _data.verts[_all2unique[i]].coord;
    }

    // linking stuff : SOFTSKIN NORMALS > DOTS & NORMAL BUFFER
    for (uint32_t i = 0; i < _data.normals_num; ++i) {

        normal_ref& nr = _unique_normals[i];
        if (nr.dot_id == UINT_MAX) {
            std::cout << "WOW! too bad, this will explode!" << std::endl;
            continue;
        }

        SoftskinNormal& sn = _data.normals[i];

        Vector3 normal_v3;
        bool feed_normal = _all_normals.size() != 0;

        std::vector<two_edge>::iterator it2e = nr.edges.begin();
        std::vector<two_edge>::iterator it2ee = nr.edges.end();

        for (; it2e != it2ee; ++it2e) {

            two_edge& te = (*it2e);
            if (te.buffer_id == UINT_MAX) {
                std::cout << "BOOM!" << std::endl;
                continue;
            }

            _data.norm_buffer[te.buffer_id] = sn.coord();

            if (feed_normal) {
                normal_v3 += _all_normals[te.buffer_id];
            }

            sn.push_back(
                    &_data.dots[ nr.dot_id ],
                    &_data.fibers[ te.edge0 ],
                    &_data.fibers[ te.edge1 ],
                    te.flip0, te.flip1);

        }
        if (feed_normal) {
            sn = normal_v3 / nr.edges.size();
        }
    }

    // ANCHORS AND LIGAMENTS
    if (!ca->_unique_anchors.empty()) {
        _data.anchors_num = ca->_unique_anchors.size();
        _data.anchors = memnew_arr(SkinAnchor, _data.anchors_num);
        size_t i = 0;
        std::vector<v3_ref>::iterator ita = ca->_unique_anchors.begin();
        std::vector<v3_ref>::iterator itae = ca->_unique_anchors.end();
        for (; ita != itae; ++ita, ++i) {
            _data.anchors[i].init((*ita).xyz, this);
        }
    }

    if (!ca->_unique_ligaments.empty()) {
        _data.ligaments_num = ca->_unique_ligaments.size();
        _data.ligaments = memnew_arr(SoftskinLigament, _data.ligaments_num);
        size_t i = 0;
        std::vector<edge_ref>::iterator itl = ca->_unique_ligaments.begin();
        std::vector<edge_ref>::iterator itle = ca->_unique_ligaments.end();
        for (; itl != itle; ++itl, ++i) {
            edge_ref& er = (*itl);
            _data.ligaments[i].init(&_data.anchors[er.v3_ref_0], &_data.dots[er.v3_ref_1]);
        }

        _enable_generate_ligaments = false;

    } else {

        _enable_generate_ligaments = true;

    }

    // GROUPS

    if (ca->_groups.empty()) {

        generate_default_groups();

    } else {

        _data.groups_num = ca->_groups.size();
        _data.groups = memnew_arr(SoftskinGroup*, _data.groups_num);

        size_t gi = 0;
        std::vector<group_ref>::iterator itg = ca->_groups.begin();
        std::vector<group_ref>::iterator itge = ca->_groups.end();
        for (; itg != itge; ++itg, ++gi) {

            group_ref& gr = (*itg);
            String gname = gr.name.c_str();

            switch (gr.type) {
                case sf_DOT:
                {
                    SoftskinGroupDot* sg = new SoftskinGroupDot();
                    sg->set_name(gname);
                    if (gr.indices.size() != gr.weights.size()) {
                        std::cout << "Softskin::generate_skinmesh, sf_DOT group:"
                                "indices and weights do not have the same "
                                "size! " <<
                                gr.indices.size() << " <> " <<
                                gr.weights.size() <<
                                std::endl;
                        _data.groups[gi] = sg;
                        continue;
                    }
                    SoftskinDot** ds = memnew_arr(SoftskinDot*, gr.indices.size());
                    real_t* ws = memnew_arr(real_t, gr.weights.size());
                    for (int i = 0, imax = gr.indices.size(); i < imax; ++i) {
                        ds[i] = &_data.dots[gr.indices[i]];
                        ws[i] = gr.weights[i];
                    }
                    sg->load(gr.indices.size(), ds, ws);
                    _data.groups[gi] = sg;
                }
                    break;
                case sf_ANCHOR:
                    std::cout << "Softskin::generate_skinmesh, sf_ANCHOR groups " << gr.name << " NOT IMPLEMENTED!" << std::endl;
                    break;
                case sf_FIBER:
                {
                    SoftskinGroupFiber* sg = new SoftskinGroupFiber();
                    sg->set_name(gname);
                    if (gr.indices.size() != gr.weights.size()) {
                        std::cout << "Softskin::generate_skinmesh, sf_FIBER group:"
                                "indices and weights do not have the same "
                                "size! " <<
                                gr.indices.size() << " <> " <<
                                gr.weights.size() <<
                                std::endl;
                        _data.groups[gi] = sg;
                        continue;
                    }
                    SoftskinFiber** ds = memnew_arr(SoftskinFiber*, gr.indices.size());
                    real_t* ws = memnew_arr(real_t, gr.weights.size());
                    for (int i = 0, imax = gr.indices.size(); i < imax; ++i) {
                        ds[i] = &_data.fibers[gr.indices[i]];
                        ws[i] = gr.weights[i];
                    }
                    sg->load(gr.indices.size(), ds, ws);
                    _data.groups[gi] = sg;
                }
                    break;
                case sf_LIGAMENT:
                {
                    SoftskinGroupLigament* sg = new SoftskinGroupLigament();
                    sg->set_name(gname);
                    if (gr.indices.size() != gr.weights.size()) {
                        std::cout << "Softskin::generate_skinmesh, sf_LIGAMENT "
                                "group: indices and weights do not have the "
                                "size! " <<
                                gr.indices.size() << " <> " <<
                                gr.weights.size() <<
                                std::endl;
                        _data.groups[gi] = sg;
                        continue;
                    }
                    SoftskinLigament** ds = memnew_arr(SoftskinLigament*, gr.indices.size());
                    real_t* ws = memnew_arr(real_t, gr.weights.size());
                    for (int i = 0, imax = gr.indices.size(); i < imax; ++i) {
                        ds[i] = &_data.ligaments[gr.indices[i]];
                        ws[i] = gr.weights[i];
                    }
                    sg->load(gr.indices.size(), ds, ws);
                    _data.groups[gi] = sg;
                }
                    break;
                default:
                    std::cout << "Unsupported group type: " << gr.name << std::endl;
                    break;

            }

        }

    }

    _data.validate();

    _buffer = ca->_surfaces[0]->buffer;
    _write_buffer = _buffer.write();

    if (_generate_ligaments && _enable_generate_ligaments) {
        set_generate_ligaments(true);
    }

    _change_notify();

}

// ********** GETTERS / SETTERS **********

Ref<Material> Softskin::get_material() {
    return _mat;
}

void Softskin::set_material(Ref<Material> m) {
    _mat = m;
    if (_mesh.is_valid() && !_mat.is_null()) {
        VS::get_singleton()->mesh_surface_set_material(_mesh, 0, _mat->get_rid());
    }
}

Ref<Material> Softskin::get_ligaments_material() {
    return _material_ligaments;
}

void Softskin::set_ligaments_material(Ref<Material> m) {
    _material_ligaments = m;
    if (_debug_ligaments != 0) {
        _debug_ligaments->set_material(m);
    }
}

Ref<Material> Softskin::get_normal_material() {
    return _material_normals;
}

void Softskin::set_normal_material(Ref<Material> m) {
    _material_normals = m;
    if (_debug_normals != 0) {
        _debug_normals->set_material(m);
    }
}

Ref<Material> Softskin::get_wireframe_material() {
    return _material_wireframe;
}

void Softskin::set_wireframe_material(Ref<Material> m) {
    _material_wireframe = m;
    if (_debug_egdes != 0) {
        _debug_egdes->set_material(m);
    }
}

const bool& Softskin::get_generate_ligaments() const {
    return _generate_ligaments;
}

void Softskin::set_generate_ligaments(const bool& b) {

    _generate_ligaments = b;

    if (!_generate_ligaments) {

        _data.purge_anchors();

    } else {

        _data.anchors_num = _data.verts_num;
        if (_data.verts_num > 0) {
            _data.anchors = memnew_arr(SkinAnchor, _data.verts_num);
        }
        // generation of anchors
        for (uint32_t i = 0; i < _data.verts_num; ++i) {
            _data.anchors[i].init(_data.dots[i].origin(), this);
        }
        // generation of ligaments
        _data.ligaments_num = _data.verts_num;
        if (_data.verts_num > 0) {
            _data.ligaments = memnew_arr(SoftskinLigament, _data.verts_num);
        }
        for (uint32_t i = 0; i < _data.verts_num; ++i) {
            _data.ligaments[i].init(&_data.anchors[i], &_data.dots[i]);
            if (_data.groups_num >= ID_GROUP_ALL_LIGAMENTS) {
                ((SoftskinGroupLigament*) _data.groups[ID_GROUP_ALL_LIGAMENTS])->push_back(&_data.ligaments[i]);
            }
        }

    }

}

const bool& Softskin::get_display_skin() const {
    return _display_skin;
}

void Softskin::set_display_skin(const bool& b) {
    _display_skin = b;
    if (_mesh.is_valid()) {
        if (_display_skin) {
            set_base(_mesh);
        } else {
            set_base(RID());
        }
    }
}

bool Softskin::get_display_ligaments() const {
    return _debug_ligaments != 0;
}

void Softskin::set_display_ligaments(const bool& b) {

    if (b && _debug_ligaments == 0) {
        _debug_ligaments = memnew(SoftskinDebug);
        _debug_ligaments->vertex_color(true);
        _debug_ligaments->line_width(_line_width);
        if (_material_ligaments.is_valid()) {
            _debug_ligaments->set_material(_material_ligaments);
        }
        add_child(_debug_ligaments);
    } else if (!b && _debug_ligaments != 0) {
        remove_child(_debug_ligaments);
        memdelete(_debug_ligaments);
        _debug_ligaments = 0;
    }

}

bool Softskin::get_display_normals() const {
    return _debug_normals != 0;
}

void Softskin::set_display_normals(const bool& b) {

    if (b && _debug_normals == 0) {
        _debug_normals = memnew(SoftskinDebug);
        _debug_normals->vertex_color(true);
        _debug_normals->line_width(_line_width);
        if (_material_normals.is_valid()) {
            _debug_normals->set_material(_material_normals);
        }
        add_child(_debug_normals);
    } else if (!b && _debug_normals != 0) {
        remove_child(_debug_normals);
        memdelete(_debug_normals);
        _debug_normals = 0;
    }

}

bool Softskin::get_display_edges() const {
    return _debug_egdes != 0;
}

void Softskin::set_display_edges(const bool& b) {

    if (b && _debug_egdes == 0) {
        _debug_egdes = memnew(SoftskinDebug);
        _debug_egdes->albedo(Color(0.7, 1.0, 0.0));
        _debug_egdes->line_width(_line_width);
        if (_material_wireframe.is_valid()) {
            _debug_egdes->set_material(_material_wireframe);
        }
        add_child(_debug_egdes);
    } else if (!b && _debug_egdes != 0) {
        remove_child(_debug_egdes);
        memdelete(_debug_egdes);
        _debug_egdes = 0;
    }

}

const float& Softskin::get_normal_length() const {
    return _normal_length;
}

void Softskin::set_normal_length(const float& b) {
    _normal_length = b;
}

const float& Softskin::get_line_width() const {
    return _line_width;
}

void Softskin::set_line_width(const float& b) {
    _line_width = b;
    if (_debug_ligaments != 0) _debug_ligaments->line_width(_line_width);
    if (_debug_normals != 0) _debug_normals->line_width(_line_width);
    if (_debug_egdes != 0) _debug_egdes->line_width(_line_width);
}

const Vector3& Softskin::get_gravity() const {
    return _gravity;
}

void Softskin::set_gravity(const Vector3& g) {
    _gravity = g;
}

SoftskinGroupDot* Softskin::get_dots(const String& name) {
    SoftskinGroup* sg = get_group(name);
    if (sg && sg->get_type() == sf_DOT) {
        return (SoftskinGroupDot*) sg;
    }
    return 0;
}

SoftskinGroupFiber* Softskin::get_fibers(const String& name) {
    SoftskinGroup* sg = get_group(name);
    if (sg && (sg->get_type() == sf_FIBER || sg->get_type() == sf_TENSOR)) {
        return (SoftskinGroupFiber*) sg;
    }
    return 0;
}

SoftskinGroupLigament* Softskin::get_ligaments(const String& name) {
    SoftskinGroup* sg = get_group(name);
    if (sg && (sg->get_type() == sf_LIGAMENT || sg->get_type() == sf_MUSCLE)) {
        return (SoftskinGroupLigament*) sg;
    }
    return 0;
}

Vector<String> Softskin::get_group_names(const softskin_type_t& t) {
    Vector<String> l;
    for (uint32_t i = 0; i < _data.groups_num; ++i) {
        if (_data.groups[i]->get_type() == t) {
            l.push_back( _data.groups[i]->get_name() );
        }
    }
    return l;
}

Vector<String> Softskin::get_dot_groups() {
    return get_group_names( sf_DOT );
}

Vector<String> Softskin::get_fiber_groups() {
    return get_group_names( sf_FIBER );
}

Vector<String> Softskin::get_ligament_groups() {
    return get_group_names( sf_LIGAMENT );
}

Vector3 Softskin::get_global_barycenter(const String& name) {
    return to_global(get_local_barycenter(name));
}

Vector3 Softskin::get_local_barycenter(const String& name) {

    SoftskinGroup* sg = get_group(name);
    if (!sg) {
        return Vector3(0, 0, 0);
    }

    switch (sg->get_type()) {
        case sf_EDGE:
            return ((SoftskinGroupEdge*) sg)->get_barycenter();
        case sf_FIBER:
        case sf_TENSOR:
            return ((SoftskinGroupFiber*) sg)->get_barycenter();
        case sf_LIGAMENT:
        case sf_MUSCLE:
            return ((SoftskinGroupFiber*) sg)->get_barycenter();
        default:
            return Vector3(0, 0, 0);
    }

}

SoftskinGroup* Softskin::get_group(const String& name, const softskin_type_t& t) const {

    SoftskinGroup* sg = 0;
    for (uint32_t i = 0; i < _data.groups_num; ++i) {
        if (
                _data.groups[i]->get_name() == name &&
                (
                t == sf_UNDEFINED ||
                _data.groups[i]->get_type() == t)
                ) {
            sg = _data.groups[i];
            break;
        }
    }
    return sg;

}

AABB Softskin::get_aabb() const {
    return _bbox;
}

PoolVector<Face3> Softskin::get_faces(uint32_t p_usage_flags) const {
    return _faces;
}

// ********** GROUPS INTERFACE **********

bool Softskin::_set(const StringName &p_path, const Variant &p_value) {

    String path = p_path;

    if (!path.begins_with(PROP_GROUPS_PREFIX))
        return false;

    String which = path.get_slicec('/', 1);
    SoftskinGroup* sg = get_group(which);
    if (!sg) return false;
    String what = path.get_slicec('/', 2);

    if (what == PROP_GROUP_STIFFNESS) {
        switch (sg->get_type()) {
            case sf_EDGE:
                ((SoftskinGroupEdge*) sg)->set_stiffness(p_value);
                break;
            case sf_FIBER:
            case sf_TENSOR:
                ((SoftskinGroupFiber*) sg)->set_stiffness(p_value);
                break;
            case sf_LIGAMENT:
            case sf_MUSCLE:
                ((SoftskinGroupLigament*) sg)->set_stiffness(p_value);
                break;
            default:
                break;
        }
    } else if (what == PROP_GROUP_REST_LEN_MULTIPLIER) {
        switch (sg->get_type()) {
            case sf_EDGE:
                ((SoftskinGroupEdge*) sg)->set_rest_len_multiplier(p_value);
                break;
            case sf_FIBER:
            case sf_TENSOR:
                ((SoftskinGroupFiber*) sg)->set_rest_len_multiplier(p_value);
                break;
            case sf_LIGAMENT:
            case sf_MUSCLE:
                ((SoftskinGroupLigament*) sg)->set_rest_len_multiplier(p_value);
                break;
            default:
                break;
        }
    } else if (what == PROP_GROUP_MUSCLE_MIN_LEN) {
        switch (sg->get_type()) {
            case sf_TENSOR:
                ((SoftskinGroupFiber*) sg)->set_muscle_min(p_value);
                break;
            case sf_MUSCLE:
                ((SoftskinGroupLigament*) sg)->set_muscle_min(p_value);
                break;
            default:
                break;
        }
    } else if (what == PROP_GROUP_MUSCLE_MAX_LEN) {
        switch (sg->get_type()) {
            case sf_TENSOR:
                ((SoftskinGroupFiber*) sg)->set_muscle_max(p_value);
                break;
            case sf_MUSCLE:
                ((SoftskinGroupLigament*) sg)->set_muscle_max(p_value);
                break;
            default:
                break;
        }
    } else if (what == PROP_GROUP_MUSCLE_FREQUENCY) {
        switch (sg->get_type()) {
            case sf_TENSOR:
                ((SoftskinGroupFiber*) sg)->set_muscle_frequency(p_value);
                break;
            case sf_MUSCLE:
                ((SoftskinGroupLigament*) sg)->set_muscle_frequency(p_value);
                break;
            default:
                break;
        }
    } else if (what == PROP_GROUP_DOT_DAMPING) {
        switch (sg->get_type()) {
            case sf_DOT:
                ((SoftskinGroupDot*) sg)->set_damping(p_value);
                break;
            default:
                break;
        }
    } else if (what == PROP_GROUP_DOT_PRESSURE) {
        switch (sg->get_type()) {
            case sf_DOT:
                ((SoftskinGroupDot*) sg)->set_pressure(p_value);
                break;
            default:
                break;
        }
    }

    return true;

}

bool Softskin::_get(const StringName &p_path, Variant &r_ret) const {

    String path = p_path;

    if (!path.begins_with(PROP_GROUPS_PREFIX))
        return false;

    String which = path.get_slicec('/', 1);
    SoftskinGroup* sg = get_group(which);
    if (!sg) return false;
    String what = path.get_slicec('/', 2);

    if (what == PROP_GROUP_NAME) {
        r_ret = sg->get_name();
    } else if (what == PROP_GROUP_TYPE) {
        r_ret = String(SoftskinEdge::print_type(sg->get_type()).c_str());
    } else if (what == PROP_GROUP_SIZE) {
        switch (sg->get_type()) {
            case sf_EDGE:
                r_ret = ((SoftskinGroupEdge*) sg)->size();
                break;
            case sf_FIBER:
            case sf_TENSOR:
                r_ret = ((SoftskinGroupFiber*) sg)->size();
                break;
            case sf_LIGAMENT:
            case sf_MUSCLE:
                r_ret = ((SoftskinGroupLigament*) sg)->size();
                break;
            case sf_DOT:
                r_ret = ((SoftskinGroupDot*) sg)->size();
                break;
            default:
                r_ret = 0;
                break;
        }
    } else if (what == PROP_GROUP_STIFFNESS) {
        switch (sg->get_type()) {
            case sf_EDGE:
                r_ret = ((SoftskinGroupEdge*) sg)->get_stiffness();
                break;
            case sf_FIBER:
            case sf_TENSOR:
                r_ret = ((SoftskinGroupFiber*) sg)->get_stiffness();
                break;
            case sf_LIGAMENT:
            case sf_MUSCLE:
                r_ret = ((SoftskinGroupLigament*) sg)->get_stiffness();
                break;
            default:
                r_ret = ssk_DEFAULT_STIFFNESS;
                break;
        }
    } else if (what == PROP_GROUP_REST_LEN_MULTIPLIER) {
        switch (sg->get_type()) {
            case sf_EDGE:
                r_ret = ((SoftskinGroupEdge*) sg)->get_rest_len_multiplier();
                break;
            case sf_FIBER:
            case sf_TENSOR:
                r_ret = ((SoftskinGroupFiber*) sg)->get_rest_len_multiplier();
                break;
            case sf_LIGAMENT:
            case sf_MUSCLE:
                r_ret = ((SoftskinGroupLigament*) sg)->get_rest_len_multiplier();
                break;
            default:
                r_ret = ssk_DEFAULT_REST_LEN_MULT;
                break;
        }
    } else if (what == PROP_GROUP_MUSCLE_MIN_LEN) {
        switch (sg->get_type()) {
            case sf_TENSOR:
                r_ret = ((SoftskinGroupFiber*) sg)->get_muscle_min();
                break;
            case sf_MUSCLE:
                r_ret = ((SoftskinGroupLigament*) sg)->get_muscle_min();
                break;
            default:
                r_ret = ssk_DEFAULT_MUSCLE_MIN_LEN;
                break;
        }
    } else if (what == PROP_GROUP_MUSCLE_MAX_LEN) {
        switch (sg->get_type()) {
            case sf_TENSOR:
                r_ret = ((SoftskinGroupFiber*) sg)->get_muscle_max();
                break;
            case sf_MUSCLE:
                r_ret = ((SoftskinGroupLigament*) sg)->get_muscle_max();
                break;
            default:
                r_ret = ssk_DEFAULT_MUSCLE_MAX_LEN;
                break;
        }
    } else if (what == PROP_GROUP_MUSCLE_FREQUENCY) {
        switch (sg->get_type()) {
            case sf_TENSOR:
                r_ret = ((SoftskinGroupFiber*) sg)->get_muscle_frequency();
                break;
            case sf_MUSCLE:
                r_ret = ((SoftskinGroupLigament*) sg)->get_muscle_frequency();
                break;
            default:
                r_ret = ssk_DEFAULT_MUSCLE_FREQUENCY;
                break;
        }
    } else if (what == PROP_GROUP_MUSCLE_FREQUENCY) {
        switch (sg->get_type()) {
            case sf_TENSOR:
                r_ret = ((SoftskinGroupFiber*) sg)->get_muscle_frequency();
                break;
            case sf_MUSCLE:
                r_ret = ((SoftskinGroupLigament*) sg)->get_muscle_frequency();
                break;
            default:
                r_ret = ssk_DEFAULT_MUSCLE_FREQUENCY;
                break;
        }
    } else if (what == PROP_GROUP_DOT_DAMPING) {
        switch (sg->get_type()) {
            case sf_DOT:
                r_ret = ((SoftskinGroupDot*) sg)->get_damping();
                break;
            default:
                r_ret = ssk_DEFAULT_DOT_DAMPING;
                break;
        }
    } else if (what == PROP_GROUP_DOT_PRESSURE) {
        switch (sg->get_type()) {
            case sf_DOT:
                r_ret = ((SoftskinGroupDot*) sg)->get_pressure();
                break;
            default:
                r_ret = ssk_DEFAULT_DOT_PESSURE;
                break;
        }
    }

    return true;

}

void Softskin::_get_property_list(List<PropertyInfo> *p_list) const {

    for (uint32_t i = 0; i < _data.groups_num; ++i) {

        SoftskinGroup* sg = _data.groups[i];

        String prep = PROP_GROUPS_PREFIX + sg->get_name() + "/";
        p_list->push_back(PropertyInfo(Variant::STRING, prep + PROP_GROUP_NAME));
        p_list->push_back(PropertyInfo(Variant::INT, prep + PROP_GROUP_SIZE));
        p_list->push_back(PropertyInfo(Variant::STRING, prep + PROP_GROUP_TYPE));
        switch (sg->get_type()) {
            case sf_EDGE:
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_STIFFNESS, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_REST_LEN_MULTIPLIER, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                break;
            case sf_FIBER:
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_STIFFNESS, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_REST_LEN_MULTIPLIER, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                break;
            case sf_TENSOR:
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_STIFFNESS, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_REST_LEN_MULTIPLIER, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_MUSCLE_MIN_LEN, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_MUSCLE_MAX_LEN, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_MUSCLE_FREQUENCY, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                break;
            case sf_LIGAMENT:
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_STIFFNESS, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_REST_LEN_MULTIPLIER, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                break;
            case sf_MUSCLE:
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_STIFFNESS, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_REST_LEN_MULTIPLIER, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_MUSCLE_MIN_LEN, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_MUSCLE_MAX_LEN, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_MUSCLE_FREQUENCY, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                break;
            case sf_DOT:
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_DOT_DAMPING, PROPERTY_HINT_RANGE, "0.0,1.0,0.00001,or_greater"));
                p_list->push_back(PropertyInfo(Variant::REAL, prep + PROP_GROUP_DOT_PRESSURE, PROPERTY_HINT_RANGE, "-1.0,1.0,0.00001,or_greater,or_lesser"));
                break;
            default:
                break;

        }
    }

}

void Softskin::_bind_methods() {

    ClassDB::bind_method(D_METHOD("proxy_changed"), &Softskin::proxy_changed);

    ClassDB::bind_method(D_METHOD("set_proxy", "proxy"), &Softskin::set_proxy);
    ClassDB::bind_method(D_METHOD("get_proxy"), &Softskin::get_proxy);

    ClassDB::bind_method(D_METHOD("set_material", "material"), &Softskin::set_material);
    ClassDB::bind_method(D_METHOD("get_material"), &Softskin::get_material);
    ClassDB::bind_method(D_METHOD("set_ligaments_material", "ligaments_material"), &Softskin::set_ligaments_material);
    ClassDB::bind_method(D_METHOD("get_ligaments_material"), &Softskin::get_ligaments_material);
    ClassDB::bind_method(D_METHOD("set_normal_material", "normals_material"), &Softskin::set_normal_material);
    ClassDB::bind_method(D_METHOD("get_normal_material"), &Softskin::get_normal_material);
    ClassDB::bind_method(D_METHOD("set_wireframe_material", "wireframe_material"), &Softskin::set_wireframe_material);
    ClassDB::bind_method(D_METHOD("get_wireframe_material"), &Softskin::get_wireframe_material);

    ClassDB::bind_method(D_METHOD("set_generate_ligaments", "generate_ligaments"), &Softskin::set_generate_ligaments);
    ClassDB::bind_method(D_METHOD("get_generate_ligaments"), &Softskin::get_generate_ligaments);

    ClassDB::bind_method(D_METHOD("set_display_skin", "display_skin"), &Softskin::set_display_skin);
    ClassDB::bind_method(D_METHOD("get_display_skin"), &Softskin::get_display_skin);
    ClassDB::bind_method(D_METHOD("set_display_ligaments", "display_ligaments"), &Softskin::set_display_ligaments);
    ClassDB::bind_method(D_METHOD("get_display_ligaments"), &Softskin::get_display_ligaments);
    ClassDB::bind_method(D_METHOD("set_display_normals", "display_normals"), &Softskin::set_display_normals);
    ClassDB::bind_method(D_METHOD("get_display_normals"), &Softskin::get_display_normals);
    ClassDB::bind_method(D_METHOD("set_display_edges", "display_edges"), &Softskin::set_display_edges);
    ClassDB::bind_method(D_METHOD("get_display_edges"), &Softskin::get_display_edges);

    ClassDB::bind_method(D_METHOD("set_normal_length", "normal_length"), &Softskin::set_normal_length);
    ClassDB::bind_method(D_METHOD("get_normal_length"), &Softskin::get_normal_length);
    ClassDB::bind_method(D_METHOD("set_line_width", "line_width"), &Softskin::set_line_width);
    ClassDB::bind_method(D_METHOD("get_line_width"), &Softskin::get_line_width);

    ClassDB::bind_method(D_METHOD("set_gravity", "gravity"), &Softskin::set_gravity);
    ClassDB::bind_method(D_METHOD("get_gravity"), &Softskin::get_gravity);

    // exposed for scripting
    ClassDB::bind_method(D_METHOD("get_dots", "name"), &Softskin::get_dots);
    ClassDB::bind_method(D_METHOD("get_fibers", "name"), &Softskin::get_fibers);
    ClassDB::bind_method(D_METHOD("get_ligaments", "name"), &Softskin::get_ligaments);
    
    ClassDB::bind_method(D_METHOD("get_dot_groups"), &Softskin::get_dot_groups);
    ClassDB::bind_method(D_METHOD("get_fiber_groups"), &Softskin::get_fiber_groups);
    ClassDB::bind_method(D_METHOD("get_ligament_groups"), &Softskin::get_ligament_groups);
    
    ClassDB::bind_method(D_METHOD("get_global_barycenter", "name"), &Softskin::get_global_barycenter);
    ClassDB::bind_method(D_METHOD("get_local_barycenter", "name"), &Softskin::get_local_barycenter);

    ADD_PROPERTY(PropertyInfo(Variant::OBJECT,
            "proxy", PROPERTY_HINT_RESOURCE_TYPE,
            "SoftskinProxy"),
            "set_proxy",
            "get_proxy");

    ADD_PROPERTY(PropertyInfo(Variant::OBJECT,
            "material", PROPERTY_HINT_RESOURCE_TYPE,
            "SpatialMaterial,ShaderMaterial"),
            "set_material",
            "get_material");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT,
            "ligaments_material", PROPERTY_HINT_RESOURCE_TYPE,
            "SpatialMaterial,ShaderMaterial"),
            "set_ligaments_material",
            "get_ligaments_material");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT,
            "normals_material", PROPERTY_HINT_RESOURCE_TYPE,
            "SpatialMaterial,ShaderMaterial"),
            "set_normal_material",
            "get_normal_material");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT,
            "wireframe_material", PROPERTY_HINT_RESOURCE_TYPE,
            "SpatialMaterial,ShaderMaterial"),
            "set_wireframe_material",
            "get_wireframe_material");


    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "generate_ligaments"),
            "set_generate_ligaments",
            "get_generate_ligaments");

    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "display_skin"),
            "set_display_skin",
            "get_display_skin");

    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "display_ligaments"),
            "set_display_ligaments",
            "get_display_ligaments");

    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "display_normals"),
            "set_display_normals",
            "get_display_normals");

    ADD_PROPERTY(PropertyInfo(Variant::REAL,
            "normal_length", PROPERTY_HINT_RANGE,
            "0.0,5.0,0.01,or_greater"),
            "set_normal_length",
            "get_normal_length");

    ADD_PROPERTY(PropertyInfo(Variant::REAL,
            "line_width", PROPERTY_HINT_RANGE,
            "0.0,5.0,0.01,or_greater"),
            "set_line_width",
            "get_line_width");

    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "display_edges"),
            "set_display_edges",
            "get_display_edges");

    ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "gravity"),
            "set_gravity",
            "get_gravity");

}

void Softskin::_validate_property(PropertyInfo &property) const {

    if (property.name == "generate_ligaments" && !_enable_generate_ligaments) {
        property.usage = 0;
    }

}

void Softskin::_notification(int p_what) {

    switch (p_what) {
        case NOTIFICATION_ENTER_TREE:
            set_process(true);
            set_process_internal(true);
            break;
        case NOTIFICATION_PROCESS:
            update_internal(get_process_delta_time());
            commit_changes();
            break;
            //        case NOTIFICATION_INTERNAL_PROCESS:
            //            update_internal(get_process_delta_time());
            //            commit_changes();
            //            break;
        default:
            break;
    }

}
