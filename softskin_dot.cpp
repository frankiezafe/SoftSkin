/*
 * 
 * 
 * _________ ____  .-. _________/ ____ .-. ____ 
 * __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                 `-'                 `-'      
 * 
 * 
 * art & game engine
 * 
 * ____________________________________  ?   ____________________________________
 *                                     (._.)
 * 
 * 
 * This file is part of softskin library
 * For the latest info, see http://polymorph.cool/
 * 
 * Copyright (c) 2018 polymorph.cool
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * ___________________________________( ^3^)_____________________________________
 * 
 * ascii font: rotated by MikeChat & myflix
 * have fun and be cool :)
 * 
 */

#include "softskin_dot.h"
#include "softskin_common.h"

SoftskinDot::SoftskinDot() :
_inititalised(false) {
}

SoftskinDot::SoftskinDot(const float& x, const float& y, const float& z) :
_inititalised(false) {
    init(x, y, z);
}

SoftskinDot::SoftskinDot(
        Vector3* vert,
        Vector3* force
        ) :
_inititalised(false) {
    init(vert, force);
}

void SoftskinDot::init(const float& x, const float& y, const float& z) {

    _vert.init(Vector3(x, y, z));
    _force.init(Vector3(0, 0, 0));
    init_internal();

}

void SoftskinDot::init(
        Vector3* vert,
        Vector3* force
        ) {

    _vert.init(vert);
    _force.init(force);
    init_internal();

}

void SoftskinDot::init_internal() {

    if (!_vert.is_initialised() || !_force.is_initialised()) {
        std::cout << "SoftskinDot::init_internal, "
                "it is mandatory to initialise vert & force to "
                "a non-null Vector3 pointer to validate a skin dot!" <<
                std::endl;
        _inititalised = false;
        return;
    }

    _vert_origin = _vert.ref();
    _total_push_length = 0;

    _damping = ssk_DEFAULT_DOT_DAMPING;
    _kicks = 0;
    _pressure = ssk_DEFAULT_DOT_PESSURE;
    _nkicks = 0;
    
    _inititalised = true;

}

void SoftskinDot::push(const Vector3& f) {
    _force += f;
    _total_push_length += f.length_squared();
    _kicks++;
}

void SoftskinDot::add_normal(const Vector3& f) {
    if ( _pressure == 0 ) {
        return;
    }
    // reseting normal at the beginning of each cycle
    if ( _nkicks == 0 ) {
        _normal *= 0;
    }
    _normal += f;
    _nkicks++;
}

const Vector3& SoftskinDot::origin() const {
    return _vert_origin;
}

const VectorPtr< Vector3 >& SoftskinDot::vert() const {
    return _vert;
}

const VectorPtr< Vector3 >& SoftskinDot::force() const {
    return _force;
}

Vector3 SoftskinDot::normal() const {
    if ( _nkicks > 0 ) {
        return _normal / _nkicks;
    }
    return _normal;
}

const float& SoftskinDot::get_damping() const {
    return _damping;
}

void SoftskinDot::set_damping(const float& d) {
    _damping = d;
}

const float& SoftskinDot::get_pressure() const {
    return _pressure;
}

void SoftskinDot::set_pressure(const float& d) {
    _pressure = d;
}

const uint16_t& SoftskinDot::kicks() const {
    return _kicks;
}

void SoftskinDot::vert(const float& x, const float& y, const float& z) {
    _vert = Vector3(x, y, z);
}

void SoftskinDot::operator=(const SoftskinDot& src) {
    _vert = src.vert();
    _force = src.force();
    _damping = src.get_damping();
    _kicks = src.kicks();
}

const Vector3& SoftskinDot::update(const float& delta_time) {

    if (_kicks < 2) {
        _kicks = 2;
    };
    
    if ( _nkicks > 0 ) {
        // normalisation of normal
        _normal /= _nkicks;
        _nkicks = 0;
        _force.ref() += _normal * _pressure;
    }

    Vector3 consumed = _force.ref() * _damping / sqrt(_kicks - 1);
    _force -= consumed;
    _vert += consumed;
    _total_push_length = _force.ref().length_squared();
    _kicks = 0;

    return _vert.ref();

}