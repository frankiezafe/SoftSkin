/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 *  
 *  This file is part of softskin library
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2018 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

#ifndef SOFTSKINCOMMON_H
#define SOFTSKINCOMMON_H

#include <iostream>
#include <climits>
#include <limits>
#include <vector>
#include <map>

#include "core/bind/core_bind.h"
#include "scene/3d/visual_instance.h"
#include "scene/resources/mesh.h"
#include "drivers/gles3/rasterizer_storage_gles3.h"

#define SOFTSKIN_VERSION 110

#define SSC_EXT "ssc"
#define SSC_NAME "SoftskinCache"
#define SSM_EXT "ssm"
#define SSM_NAME "SoftskinMesh"

#define SSC_MARKER_SIZE 10
#define SSC_SSMSTARTMA "ssmstartma"
#define SSC_SURFACEMAR "surfacemar"
#define SSC_ARRAYMARKE "arraymarke"
#define SSC_ALL2UNIQUE "all2unique"
#define SSC_UNIQUEDOTS "uniquedots"
#define SSC_UNIQUEANCH "uniqueanch"
#define SSC_UNIQUEFIBE "uniquefibe"
#define SSC_UNIQUELIGA "uniqueliga"
#define SSC_FACEALLVER "faceallver"
#define SSC_FACEFIBERM "facefiberm"
#define SSC_UNIQUENORM "uniquenorm"
#define SSC_ANCHORMARK "anchormark"
#define SSC_GROUPMARKE "groupmarke"
#define SSC_END_MARKER "end_marker"

#define VERTICE_MERGE_DISTANCE 1e-5

// default edge configuration
#define ssk_DEFAULT_CURRENT_LEN 0
#define ssk_DEFAULT_REST_LEN 0
#define ssk_DEFAULT_REST_LEN_MULT 1
#define ssk_DEFAULT_INIT_REST_LEN 0
#define ssk_DEFAULT_STIFFNESS 1
#define ssk_DEFAULT_MUSCLE_MIN_LEN 0
#define ssk_DEFAULT_MUSCLE_MAX_LEN 1
#define ssk_DEFAULT_MUSCLE_PHASE_SHIFT 1
#define ssk_DEFAULT_MUSCLE_FREQUENCY 0
#define ssk_DEFAULT_DOT_DAMPING 1
#define ssk_DEFAULT_DOT_PESSURE 0

// groups default names
#define PROP_GROUPS_PREFIX              "groups/"
#define PROP_GROUP_NAME                 "name"
#define PROP_GROUP_SIZE                 "size"
#define PROP_GROUP_TYPE                 "type"
#define PROP_GROUP_STIFFNESS            "stiffness"
#define PROP_GROUP_REST_LEN_MULTIPLIER  "shrink"
#define PROP_GROUP_MUSCLE_MIN_LEN       "muscle_min"
#define PROP_GROUP_MUSCLE_MAX_LEN       "muscle_max"
#define PROP_GROUP_MUSCLE_FREQUENCY     "muscle_freq"
#define PROP_GROUP_DOT_DAMPING          "dot_damping"
#define PROP_GROUP_DOT_PRESSURE         "dot_pressure"

#define ID_GROUP_ALL_FIBERS             0
#define ID_GROUP_ALL_LIGAMENTS          ID_GROUP_ALL_FIBERS + 1
#define ID_GROUP_ALL_DOTS               ID_GROUP_ALL_LIGAMENTS + 1
#define ID_GROUP_MAX                    ID_GROUP_ALL_DOTS + 1
//#define ID_GROUP_ALL_TENSORS            ID_GROUP_ALL_FIBERS + 1
//#define ID_GROUP_ALL_MUSCLES            ID_GROUP_ALL_LIGAMENTS + 1
#define NAME_GROUP_ALL_FIBERS           "_all_fibers"
#define NAME_GROUP_ALL_LIGAMENTS        "_all_ligaments"
#define NAME_GROUP_ALL_DOTS             "_all_dots"
//#define NAME_GROUP_ALL_TENSROS          "_all_tensors"
//#define NAME_GROUP_ALL_MUSCLES          "_all_muscles"

#define SS_DEBUG_NORMAL_LENGTH          0.1
#define SS_DEBUG_LINE_WIDTH             1.0

#define __ss_float_size sizeof(float)
#define __ss_size_t_size sizeof(size_t)
#define __ss_int_size sizeof(int)
#define __ss_uint32_size sizeof(uint32_t)
#define __ss_int32_size sizeof(int32_t)
#define __ss_str_size 255

typedef std::map<uint32_t, uint32_t> ss_unique_map;
typedef std::vector<uint32_t> ss_unique_vec;
typedef std::map<uint32_t, std::vector<uint32_t> > ss_face_map;

enum softskin_type_t {
    sf_UNDEFINED = 0, // the name is explicit: undefined type
    sf_DOT = 1, // used to flag objects related to SkinDot
    sf_ANCHOR = 2, // used to flag objects related to SkinAnchor
    sf_EDGE = 3, // edge default type, connecting 2 dots
    sf_FIBER = 4, // edge connecting 2 dots, passive behaviour
    sf_TENSOR = 5, // edge connecting 2 dots, able to contract
    sf_LIGAMENT = 6, // edge connecting 1 dots to 1 anchor, passive behaviour
    sf_MUSCLE = 7 // edge connecting 1 dots to 1 anchor, able to contract
};

union ss_float_uint8 {
    float v;
    uint8_t* bytes;

    ss_float_uint8() {
        bytes = new uint8_t[__ss_float_size];
    }
};

union ss_size_uint8 {
    size_t v;
    uint8_t* bytes;

    ss_size_uint8() {
        bytes = new uint8_t[__ss_size_t_size];
    }
};

union ss_uint32_uint8 {
    uint32_t v;
    uint8_t* bytes;

    ss_uint32_uint8() {
        bytes = new uint8_t[__ss_uint32_size];
    }
};

union ss_int32_uint8 {
    int32_t v;
    uint8_t* bytes;

    ss_int32_uint8() {
        bytes = new uint8_t[__ss_int32_size];
    }
};

union ss_int_uint8 {
    int v;
    uint8_t* bytes;

    ss_int_uint8() {
        bytes = new uint8_t[__ss_int_size];
    }
};

union ss_string_uint8 {
    char* v;
    uint8_t* bytes;

    ss_string_uint8() {
        v = new char[__ss_str_size];
        bytes = new uint8_t[__ss_str_size];
    }
};

union ss_marker_uint8 {
    char* v;
    uint8_t* bytes;

    ss_marker_uint8() {
        v = new char[SSC_MARKER_SIZE];
        bytes = new uint8_t[SSC_MARKER_SIZE];
    }
};

#endif // SOFTSKINCOMMON_H
