import os
import bpy
from bpy_extras.io_utils import ExportHelper
from bpy.props import (
	EnumProperty,
	BoolProperty,
	FloatProperty,
	IntProperty,
	StringProperty
)

from . import constants

bl_info = {
	'name': "Softskin SSM Format",
	'author': "frankiezafe",
	'version': (1,0,0),
	'blender': (2, 80, 0),
	'location': "File > Import-Export",
	'description': "Export binary meshes for softskin",
	'warning': "",
	'wiki_url': "https://gitlab.com/frankiezafe/SoftSkin/wikis/home",
	'tracker_url': "https://gitlab.com/frankiezafe/SoftSkin/issues",
	'category': 'Import-Export'
}

if "bpy" in locals():
	import imp
	if "export_ssm" in locals():
		imp.reload(export_ssm)

class ExportSSM(bpy.types.Operator, ExportHelper):
	
	"""Tooltip"""
	bl_idname = "export_object.ssm"
	bl_label = "Export SSM"
	bl_options = {"PRESET"}
	
	filename_ext = ".%s" % constants.SSM_EXT
	filter_glob = StringProperty(default="*.%s" % constants.SSM_EXT, options={"HIDDEN"})

	dump_data = BoolProperty(
		name="Console dump",
		description="Dumps an overview of the data in the terminal.",
		default=False,
	)
	
	generate_json = BoolProperty(
		name="JSON",
		description="Generate a JSON file containing the same info as the binary.",
		default=False,
	)
	
	@property
	def check_extension(self):
		return True

	def execute(self, context):
		
		if not self.filepath:
			raise Exception("filepath not set")
			return
		
		if not bpy.context.active_object:
			raise Exception("no object selected")
			return
		
		from . import export_ssm
		exporter = export_ssm.SSMesh()
		exporter.__init__()
		if exporter.load(context.active_object, self.properties.dump_data ):
			if exporter.save(self.filepath, self.properties.generate_json ):
				return {'FINISHED'}
		return {'CANCELLED'}

def menu_func_export(self, context):
	default_path = bpy.data.filepath.replace('.blend', constants.SSM_EXT)
	self.layout.operator(ExportSSM.bl_idname, text="Softskin mesh (.%s)" % constants.SSM_EXT )
	#operator.filepath = default_path

def register():
	bpy.utils.register_class(ExportSSM)
	bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
	bpy.utils.unregister_class(ExportSSM)
	bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

if __name__ == "__main__":
	register()
