/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_resource.h
 * Author: frankiezafe
 *
 * Created on July 21, 2019, 12:22 AM
 */

#ifndef SOFTSKIN_RESOURCE_H
#define SOFTSKIN_RESOURCE_H

#include <cstring>
#include <sstream>
#include <map>

#include "core/resource.h"
#include "scene/resources/mesh.h"

#include "softskin_common.h"
#include "softskin_data.h"

class SkinResource : public Resource {
    GDCLASS(SkinResource, Resource);
    OBJ_CATEGORY("Softskin");

public:
    
    SkinResource();
    virtual ~SkinResource();
    
    bool compare(PoolVector<Vector3>& local, const PoolVector<Vector3>& foreign);
    bool compare(PoolVector<float>& local, const PoolVector<float>& foreign);
    bool compare(PoolVector<int>& local, const PoolVector<int>& foreign);

protected:

    static void _bind_methods();

    // converters
    ss_float_uint8 f2u;
    ss_size_uint8 sz2u;
    ss_int_uint8 i2u;
    ss_uint32_uint8 ui32u;
    ss_int32_uint8 i32u;
    ss_string_uint8 st2u;
    ss_marker_uint8 st10u;

    bool is_marker(uint8_t*, const char*);
    bool is_marker(FileAccess* f, const char*);

    void save_marker(FileAccess* f, const char*);
    void save_value(FileAccess* f, const char*);
    void save_value(FileAccess* f, const float&);
    void save_value(FileAccess* f, const int&);
    void save_value(FileAccess* f, const size_t&);
    void save_value(FileAccess* f, const uint32_t&);
    void save_poolvector(FileAccess* f, PoolVector<uint8_t>&);
    void save_poolvector(FileAccess* f, PoolVector<Vector3>&);
    void save_poolvector(FileAccess* f, PoolVector<Vector2>&);
    void save_poolvector(FileAccess* f, PoolVector<Color>&);
    void save_poolvector(FileAccess* f, PoolVector<float>&);
    void save_poolvector(FileAccess* f, PoolVector<int>&);
    void save_stdvector(FileAccess* f, ss_unique_vec&);

    void load_marker(FileAccess* f, std::string&);
    void load_value(FileAccess* f, std::string&);
    void load_value(FileAccess* f, float&);
    void load_value(FileAccess* f, int&);
    void load_value(FileAccess* f, size_t&);
    void load_value(FileAccess* f, uint32_t&);
    void load_poolvector(FileAccess* f, const size_t&, PoolVector<uint8_t>&);
    void load_poolvector(FileAccess* f, const size_t&, PoolVector<Vector3>&);
    void load_poolvector(FileAccess* f, const size_t&, PoolVector<Vector2>&);
    void load_poolvector(FileAccess* f, const size_t&, PoolVector<Color>&);
    void load_poolvector(FileAccess* f, const size_t&, PoolVector<float>&);
    void load_poolvector(FileAccess* f, const size_t&, PoolVector<int>&);
    void load_stdvector(FileAccess* f, const size_t&, ss_unique_vec&);

};

#endif /* SOFTSKIN_RESOURCE_H */

