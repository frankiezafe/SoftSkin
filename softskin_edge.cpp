/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "softskin_edge.h"

std::string SoftskinEdge::print_type(const softskin_type_t& t) {
    switch (t) {
        case sf_UNDEFINED:
            return "sf_UNDEFINED";
        case sf_DOT:
            return "sf_DOT";
        case sf_ANCHOR:
            return "sf_ANCHOR";
        case sf_EDGE:
            return "sf_EDGE";
        case sf_FIBER:
            return "sf_FIBER";
        case sf_TENSOR:
            return "sf_TENSOR";
        case sf_LIGAMENT:
            return "sf_LIGAMENT";
        case sf_MUSCLE:
            return "sf_MUSCLE";
        default:
            return "INVALID TYPE!";
    }
}

SoftskinEdge::SoftskinEdge() :
_type(sf_EDGE),
_current_len(ssk_DEFAULT_CURRENT_LEN),
_rest_len(ssk_DEFAULT_REST_LEN),
_rest_len_multiplier(ssk_DEFAULT_REST_LEN_MULT),
_init_rest_len(ssk_DEFAULT_INIT_REST_LEN),
_stiffness(ssk_DEFAULT_STIFFNESS),
_force_feedback(0, 0, 0),
_muscled(false),
_muscle_min_len(0),
_muscle_max_len(0),
_muscle_delta_len(0),
_muscle_a(0),
_muscle_phase_shift(ssk_DEFAULT_MUSCLE_PHASE_SHIFT),
_muscle_frequency(ssk_DEFAULT_MUSCLE_FREQUENCY) {
    set_muscle_min_max( ssk_DEFAULT_MUSCLE_MIN_LEN, ssk_DEFAULT_MUSCLE_MAX_LEN );
}

const softskin_type_t& SoftskinEdge::get_type() const {
    return _type;
}

const Vector3& SoftskinEdge::get_dir() const {
    return _dir;
}

const Vector3& SoftskinEdge::get_middle() const {
    return _middle;
}

const float& SoftskinEdge::get_current_len() const {
    return _current_len;
}

const float& SoftskinEdge::get_rest_len() const {
    return _rest_len;
}

const float& SoftskinEdge::get_rest_len_multiplier() const {
    return _rest_len_multiplier;
}

const float& SoftskinEdge::get_init_rest_len() const {
    return _init_rest_len;
}

const float& SoftskinEdge::get_stiffness() const {
    return _stiffness;
}

const float& SoftskinEdge::get_muscle_min() const {
    return _muscle_min_len_requested;
}

const float& SoftskinEdge::get_muscle_max() const {
    return _muscle_max_len;
}

const bool& SoftskinEdge::get_muscle() const {
    return _muscled;
}

const float& SoftskinEdge::get_muscle_frequency() const {
    return _muscle_frequency;
}

void SoftskinEdge::set_len(const float& l) {
    _rest_len = l;
    _init_rest_len = l;
}

void SoftskinEdge::set_rest_len(const float& l) {
    _rest_len = l;
}

void SoftskinEdge::set_rest_len_multiplier(const float& l) {
    _rest_len_multiplier = l;
}

void SoftskinEdge::set_stiffness(const float& s) {
    _stiffness = s;
}

void SoftskinEdge::set_muscle_min_max(float min, float max) {
    _muscle_delta_len = (max - min) * 0.5;
    _muscle_min_len_requested = min;
    _muscle_min_len = min + _muscle_delta_len;
    _muscle_max_len = max;
}

void SoftskinEdge::set_muscle_min(float min) {
    set_muscle_min_max( min, _muscle_max_len );
}

void SoftskinEdge::set_muscle_max(float max) {
    set_muscle_min_max( _muscle_min_len_requested, max );
}

void SoftskinEdge::set_muscle_frequency(float f) {
    _muscle_frequency = f;
}

void SoftskinEdge::set_muscle_phase_shift(float shift) {
    _muscle_phase_shift = shift;
}

bool SoftskinEdge::muscle(bool enable) {
    if (
            _type == sf_EDGE ||
            (enable && _type != sf_FIBER && _type != sf_LIGAMENT) ||
            (!enable && _type != sf_TENSOR && _type != sf_MUSCLE)
            ) {
        std::cout << "set_muscle, muscle can not be set to " << enable << std::endl;
        return false;
    }
    // ok to set the muscle
    _muscled = enable;
    _muscle_max_len = _init_rest_len;
    // changing edge type
    if (_muscled && _type == sf_FIBER) {
        _type = sf_TENSOR;
    } else if (_muscled && _type == sf_LIGAMENT) {
        _type = sf_MUSCLE;
    } else if (!_muscled && _type == sf_TENSOR) {
        _type = sf_FIBER;
    } else if (!_muscled && _type == sf_MUSCLE) {
        _type = sf_LIGAMENT;
    }
    return true;
}

bool SoftskinEdge::musclise(float min, float max, float freq, float shift) {

    if (!muscle(true)) {
        return false;
    }
    _muscle_a = 0;
    _muscled = true;
    set_muscle_min_max(min, max);
    set_muscle_frequency(freq);
    set_muscle_phase_shift(shift);
    return true;

}

void SoftskinEdge::update_muscle(float delta_time) {
    _muscle_a += delta_time * Math_TAU * 2 * _muscle_frequency;
    _rest_len =
            _muscle_min_len +
            sin(_muscle_phase_shift + _muscle_a) *
            _muscle_delta_len;
}

const Vector3& SoftskinEdge::update(const float& delta_time) {
    if (_type != sf_EDGE) {
        if (_muscled) {
            update_muscle(delta_time);
        }
        update_specialised();
    }
    return _force_feedback;
}