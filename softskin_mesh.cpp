/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   soft_mesh.cpp
 * Author: frankiezafe
 * 
 * Created on July 20, 2019, 11:43 PM
 */

#include "softskin_mesh.h"

SoftskinMesh::SoftskinMesh() {
}

SoftskinMesh::~SoftskinMesh() {
}

void SoftskinMesh::purge() {

    _anchors.resize(0);
    _dots.resize(0);
    _fibers.resize(0);
    _ligaments.resize(0);
    _groups.resize(0);
    _extra_uvs.resize(0);
    _normals.resize(0);
    _colors.resize(0);
    _uvs.resize(0);
    _uv2s.resize(0);
    _indices.resize(0);
    _unique_normals.resize(0);

}

void SoftskinMesh::_bind_methods() {
}

Error SoftskinMesh::load_file(const String &p_path) {

    Error error_file;
    FileAccess* f = FileAccess::open(p_path, FileAccess::READ, &error_file);

    if (!f) {
        std::cout << "SSM, read: cannot access file..." << std::endl;
        return FAILED;
    }

    // back to beginning
    f->seek(0);

    std::string smarker;
    int imarker;
    load_value(f, smarker);
    load_value(f, imarker);
    
    bool valid = true;

    if (smarker.compare(SSM_EXT) == -1) {
        std::cout << "SoftskinMesh::load_file, WRONG FILE TYPE!" << std::endl;
        valid = false;
    } else if (imarker != SOFTSKIN_VERSION) {
        std::cout << "SoftskinMesh::load_file, " << imarker << " WRONG VERSION, currently supported " << SOFTSKIN_VERSION << std::endl;
        valid = false;
    }

    if (valid) {
        purge();
        if (!deserialise(f)) {
            std::cout << "SoftskinCache::load_file, PARSING WENT WRONG!" << std::endl;
            purge();
        } else if (_cache.is_valid()) {
            // invalidation of cache, will be regenerated at next get_cache call
            _cache.unref();
        }
    }

    f->close();
    memdelete(f);

    emit_changed();

    return OK;

}

void SoftskinMesh::dump(bool deep) {

    std::cout << "SoftskinMesh dump ********************** " << std::endl;

    std::cout << "\t" << "anchors: " << _anchors.size() << std::endl;
    if (deep) {
        for (int i = 0; i < _anchors.size(); ++i) {
            std::cout << "\t\t" << (i * 3) << " : " << _anchors[i][0] << std::endl;
            std::cout << "\t\t" << (i * 3 + 1) << " : " << _anchors[i][1] << std::endl;
            std::cout << "\t\t" << (i * 3 + 2) << " : " << _anchors[i][2] << std::endl;
        }
    }
    std::cout << "\t" << "dots: " << _dots.size() << std::endl;
    if (deep) {
        for (int i = 0; i < _dots.size(); ++i) {
            std::cout << "\t\t" << (i * 3) << " : " << _dots[i][0] << std::endl;
            std::cout << "\t\t" << (i * 3 + 1) << " : " << _dots[i][1] << std::endl;
            std::cout << "\t\t" << (i * 3 + 2) << " : " << _dots[i][2] << std::endl;
        }
    }
    std::cout << "\t" << "face_fibers: " << _face_fibers.size() << std::endl;
    if (deep) {
        for (int i = 0; i < _face_fibers.size(); i += 3) {
            std::cout << "\t\t" << (i / 3) << " : " <<
                    _face_fibers[i] << ", " <<
                    _face_fibers[i + 1] << ", " <<
                    _face_fibers[i + 2] << std::endl;
        }
    }
    std::cout << "\t" << "fibers: " << _fibers.size() << std::endl;
    std::cout << "\t" << "ligaments: " << _ligaments.size() << std::endl;
    std::cout << "\t" << "groups: " << _groups.size() << std::endl;
    if (deep) {
        for (size_t i = 0; i < _groups.size(); ++i) {
            group_ref& gr = _groups[i];
            std::cout << "\t\t** " << gr.name << " **" << std::endl;
            std::cout << "\t\t" << "type: " << gr.type << std::endl;
            std::cout << "\t\t" << "indices: " << gr.indices.size() << std::endl;
            std::cout << "\t\t" << "weights: " << gr.weights.size() << std::endl;
        }
    }
    std::cout << "\t" << "extra_uvs: " << _extra_uvs.size() << std::endl;
    if (deep) {
        for (int i = 0; i < _extra_uvs.size(); ++i) {
            std::cout << "\t\t" << i << ", " << _extra_uvs[i].size() << std::endl;
        }
    }
    std::cout << "\t" << "vertices: " << _vertices.size() << std::endl;
    if (deep) {
        for (int i = 0; i < _vertices.size(); ++i) {
            std::cout << "\t\t" << (i * 3) << " : " << _vertices[i][0] << std::endl;
            std::cout << "\t\t" << (i * 3 + 1) << " : " << _vertices[i][1] << std::endl;
            std::cout << "\t\t" << (i * 3 + 2) << " : " << _vertices[i][2] << std::endl;
        }
    }
    std::cout << "\t" << "normals: " << _normals.size() << std::endl;
    if (deep) {
        for (int i = 0; i < _normals.size(); ++i) {
            std::cout << "\t\t" << (i * 3) << " : " << _normals[i][0] << std::endl;
            std::cout << "\t\t" << (i * 3 + 1) << " : " << _normals[i][1] << std::endl;
            std::cout << "\t\t" << (i * 3 + 2) << " : " << _normals[i][2] << std::endl;
        }
    }
    std::cout << "\t" << "tangents: " << _tangents.size() << std::endl;
    std::cout << "\t" << "colors: " << _colors.size() << std::endl;
    std::cout << "\t" << "uvs: " << _uvs.size() << std::endl;
    std::cout << "\t" << "uv2s: " << _uv2s.size() << std::endl;
    std::cout << "\t" << "indices: " << _indices.size() << std::endl;
    if (deep) {
        for (int i = 0; i < _indices.size(); ++i) {
            std::cout << "\t\t" << i << " : " << _indices[i] << std::endl;
        }
    }
    std::cout << "\t" << "unique_normals: " << _unique_normals.size() << std::endl;
    if (deep) {
        for (size_t i = 0; i < _unique_normals.size(); ++i) {

            std::cout << "\t\t" << i << ", faces: " << _unique_normals[i].faces.size() <<
                    ", edges: " << _unique_normals[i].edges.size()
                    << std::endl;
        }
    }

}

bool SoftskinMesh::deserialise(FileAccess* f) {

    if (!is_marker(f, SSC_SSMSTARTMA)) {
        std::cout << "SoftskinMesh::load_mesh SSC_SSMSTARTMA" << std::endl;
        return false;
    }

    int32_t size;
    load_value(f, size);
    load_poolvector(f, size / 3, _anchors);

    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh anchors " << size << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size / 3, _dots);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh _dots" << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size / 3, _normals);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh _normals " << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size / 3, _tangents);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh _tangents " << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size / 4, _colors);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh ARRAY_COLOR " << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size / 2, _uvs);
    if (!is_marker(f, SSC_END_MARKER)) {
        dump();
        std::cout << "SoftskinMesh::load_mesh ARRAY_TEX_UV " << f->get_position() << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size / 2, _uv2s);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh ARRAY_TEX_UV2 " << std::endl;
        return false;
    }

    load_value(f, size);
    _extra_uvs.resize(size);
    for (int i = 0; i < size; ++i) {
        load_value(f, size);
        load_poolvector(f, size / 2, _extra_uvs.write()[i]);
        if (!is_marker(f, SSC_END_MARKER)) {
            std::cout << "SoftskinMesh::load_mesh extra_uvs " << i << std::endl;
            return false;
        }
    }

    load_value(f, size);
    load_poolvector(f, size, _indices);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh _indices " << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size, _face_fibers);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh _face_edges " << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size, _fibers);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh fibers " << std::endl;
        return false;
    }

    load_value(f, size);
    load_poolvector(f, size, _ligaments);
    if (!is_marker(f, SSC_END_MARKER)) {
        std::cout << "SoftskinMesh::load_mesh ligaments " << std::endl;
        return false;
    }

    load_value(f, size);
    _groups.resize(size);
    for (int i = 0; i < size; ++i) {
        group_ref& gr = _groups[i];
        load_value(f, gr.name);
        int32_t gtype;
        int32_t indices_num;
        int32_t weights_num;
        load_value(f, gtype);
        gr.type = softskin_type_t(gtype);
        load_value(f, indices_num);
        load_poolvector(f, indices_num, gr.indices);
        if (!is_marker(f, SSC_END_MARKER)) {
            std::cout << "SoftskinMesh::load_mesh groups.indices " << " / " << i << std::endl;
            return false;
        }
        load_value(f, weights_num);
        load_poolvector(f, weights_num, gr.weights);
        if (!is_marker(f, SSC_END_MARKER)) {
            std::cout << "SoftskinMesh::load_mesh groups.weights " << i << std::endl;
            return false;
        }

    }

    load_value(f, size);
    ss_unique_vec norm_edges;
    _unique_normals.resize(size);

    int32_t normedge_num;
    for (int i = 0; i < size; ++i) {

        load_value(f, normedge_num);
        load_stdvector(f, normedge_num, norm_edges);
        if (!is_marker(f, SSC_END_MARKER)) {
            std::cout << "SoftskinMesh::load_mesh crashed while deserialising "
                    "norm_edges " << i << std::endl;
            return false;
        }
        
        // witch dot to inform?
        _unique_normals[i].dot_id = norm_edges[0];
        
        for (int j = 1; j < normedge_num; j += 5) {
            // info about the buffer position and the fibers to use
            two_edge te;
            te.buffer_id =  norm_edges[j];
            te.edge0 =      norm_edges[j + 1];
            te.flip0 =      (norm_edges[j + 2] == 1);
            te.edge1 =      norm_edges[j + 3];
            te.flip1 =      (norm_edges[j + 4] == 1);
            _unique_normals[i].edges.push_back(te);
        }

    }

    return true;

}

Ref<SoftskinCache> SoftskinMesh::get_cache() {

    if (_cache.is_valid()) {
        return _cache;
    }

    // complete mesh loading

    SoftskinCache* tmp = memnew(SoftskinCache);
    _cache = Ref<SoftskinCache>(tmp);

    int dot_num = _dots.size();
    int anchor_num = _anchors.size();
    int index_num = _indices.size();

    int fiber_num = _fibers.size() / 2;
    int ligament_num = _ligaments.size() / 2;

    SoftskinCache::array_decompressor arrdc;
    arrdc.ARRAY_VERTEX.resize(index_num);
    arrdc.ARRAY_NORMAL.resize(index_num);
    arrdc.ARRAY_INDEX.resize(index_num);

    bool normal_present = _normals.size() != 0;
    bool tangents_present = _tangents.size() != 0;
    bool colors_present = _colors.size() != 0;
    bool uv_present = _uvs.size() != 0;
    bool uv2_present = _uv2s.size() != 0;

    if (tangents_present) {
        arrdc.ARRAY_TANGENT.resize(index_num);
    }
    if (colors_present) {
        arrdc.ARRAY_COLOR.resize(index_num);
    }
    if (uv_present) {
        arrdc.ARRAY_TEX_UV.resize(index_num);
    }
    if (uv2_present) {
        arrdc.ARRAY_TEX_UV2.resize(index_num);
    }

    // writers
    PoolVector<Vector3>::Write ARRAY_VERTEX = arrdc.ARRAY_VERTEX.write();
    PoolVector<Vector3>::Write ARRAY_NORMAL = arrdc.ARRAY_NORMAL.write();
    PoolVector<real_t>::Write ARRAY_TANGENT = arrdc.ARRAY_TANGENT.write();
    PoolVector<Color>::Write ARRAY_COLOR = arrdc.ARRAY_COLOR.write();
    PoolVector<Vector2>::Write ARRAY_TEX_UV = arrdc.ARRAY_TEX_UV.write();
    PoolVector<Vector2>::Write ARRAY_TEX_UV2 = arrdc.ARRAY_TEX_UV2.write();
    PoolVector<int>::Write ARRAY_INDEX = arrdc.ARRAY_INDEX.write();

    tmp->_unique_anchors.resize(anchor_num);
    tmp->_unique_dots.resize(dot_num);
    tmp->_unique_fibers.resize(fiber_num);
    tmp->_unique_ligaments.resize(ligament_num);

    // anchors
    for (int i = 0; i < anchor_num; ++i) {
        v3_ref& vr = tmp->_unique_anchors[i];
        vr.belong_2_face = false;
        vr.local_id = i;
        vr.xyz = _anchors[i];
    }

    // dots
    for (int i = 0; i < dot_num; ++i) {
        v3_ref& vr = tmp->_unique_dots[i];
        vr.belong_2_face = true;
        vr.local_id = i;
        vr.xyz = _dots[i];
    }

    // preparing fibers
    for (int i = 0; i < fiber_num; ++i) {
        edge_ref& er = tmp->_unique_fibers[i];
        er.v3_ref_0 = _fibers[i * 2];
        er.v3_ref_1 = _fibers[i * 2 + 1];
        er.ligament = false;
    }

    // preparing ligaments
    for (int i = 0; i < ligament_num; ++i) {
        edge_ref& er = tmp->_unique_ligaments[i];
        er.v3_ref_0 = _ligaments[i * 2]; // << anchor!!!
        er.v3_ref_1 = _ligaments[i * 2 + 1];
        er.ligament = true;
    }

    for (int i = 0; i < index_num; i += 3) {

        int fid = i / 3;

        for (int pt = 0; pt < 3; ++pt) {

            int pti = pt + i;
            int dot_id = _indices[pti];
            int fib_id = _face_fibers[ pti ];

            tmp->_all2unique[ pti ] = dot_id;
            tmp->_unique_dots[ dot_id ].mirrors.push_back(pti);
            tmp->_unique_fibers[ fib_id ].faces.push_back(fid);

            ARRAY_VERTEX[pti] = _dots[ dot_id ];
            ARRAY_INDEX[pti] = pti;

            if (normal_present) {
                ARRAY_NORMAL[pti] = _normals[ dot_id ];
            }
            if (tangents_present) {
                ARRAY_TANGENT[pti] = _tangents[ dot_id ];
            }
            if (colors_present) {
                ARRAY_COLOR[pti] = _colors[ dot_id ];
            }
            if (uv_present) {
                ARRAY_TEX_UV[pti] = _uvs[ pti ];
            }
            if (uv2_present) {
                ARRAY_TEX_UV2[pti] = _uv2s[ pti ];
            }

        }

    }
    
    tmp->_unique_normals = _unique_normals;
    tmp->_groups = _groups;

    SoftskinCache::MeshArray* ma = memnew(SoftskinCache::MeshArray);
    tmp->_surfaces.resize(1);
    tmp->_surfaces[0] = ma;
    tmp->decompressor_to_array(arrdc, ma->arr);

    RID tmp_mesh = VS::get_singleton()->mesh_create();
    VS::get_singleton()->mesh_add_surface_from_arrays(tmp_mesh, VS::PRIMITIVE_TRIANGLES, ma->arr);

    int SURF_ID = 0;
    ma->surface_format = VS::get_singleton()->mesh_surface_get_format(tmp_mesh, SURF_ID);
    ma->primitive_type = VS::get_singleton()->mesh_surface_get_primitive_type(tmp_mesh, SURF_ID);
    const int surface_vertex_len = VS::get_singleton()->mesh_surface_get_array_len(tmp_mesh, SURF_ID);
    const int surface_index_len = VS::get_singleton()->mesh_surface_get_array_index_len(tmp_mesh, SURF_ID);
    uint32_t surface_offsets[VS::ARRAY_MAX];
    ma->stride = VS::get_singleton()->mesh_surface_make_offsets_from_format(ma->surface_format, surface_vertex_len, surface_index_len, surface_offsets);
    ma->offset_vertices = surface_offsets[VS::ARRAY_VERTEX];
    ma->offset_normal = surface_offsets[VS::ARRAY_NORMAL];
    ma->offset_uv = surface_offsets[VS::ARRAY_TEX_UV];
    ma->buffer = VS::get_singleton()->mesh_surface_get_array(tmp_mesh, SURF_ID);
    ma->arr = VS::get_singleton()->mesh_surface_get_arrays(tmp_mesh, SURF_ID);

    ma->verts = arrdc.ARRAY_VERTEX;
    ma->normals = arrdc.ARRAY_NORMAL;
    ma->indices = arrdc.ARRAY_INDEX;

    VS::get_singleton()->mesh_clear(tmp_mesh);

    return _cache;

}