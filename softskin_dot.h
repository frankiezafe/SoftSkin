/*
 * 
 * 
 * _________ ____  .-. _________/ ____ .-. ____ 
 * __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                 `-'                 `-'      
 * 
 * 
 * art & game engine
 * 
 * ____________________________________  ?   ____________________________________
 *                                     (._.)
 * 
 * 
 * This file is part of softskin library
 * For the latest info, see http://polymorph.cool/
 * 
 * Copyright (c) 2018 polymorph.cool
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * ___________________________________( ^3^)_____________________________________
 * 
 * ascii font: rotated by MikeChat & myflix
 * have fun and be cool :)
 * 
 */


#ifndef SOFTSKINDOT_H
#define SOFTSKINDOT_H

#include <iostream>
#include <ostream>
#include <vector>
#include "core/math/vector3.h"

#include "vector_ptr.h"
#include "softskin_common.h"

// in case there is more than 1 usage of the vertice
// in the mesh

class SoftskinDot {
public:

    // do not forget to initialised after calling this constructor!!
    SoftskinDot();

    SoftskinDot(const float& x, const float& y, const float& z);

    SoftskinDot(Vector3* vert, Vector3* force);

    void init(const float& x, const float& y, const float& z);

    void init(Vector3* vert, Vector3* force);

    void push(const Vector3& f);
    
    void add_normal(const Vector3& f);
    
    const Vector3& origin() const;

    // behaviour
    
    const float& get_damping() const;
    
    void set_damping(const float& d);
    
    const float& get_pressure() const;
    
    void set_pressure(const float& d);
    
    const VectorPtr< Vector3 >& vert() const;

    const VectorPtr< Vector3 >& force() const;
    
    Vector3 normal() const;

    const uint16_t& kicks() const;

    void vert(const float& x, const float& y, const float& z);

    const Vector3& update(const float& delta_time);

    _FORCE_INLINE_ const bool is_initialised() const {
        return _inititalised;
    }

    void operator=(const SoftskinDot& src);

    friend std::ostream &operator<<(
            std::ostream &os,
            SoftskinDot const &sd
            ) {

        return os <<
                sd.vert()[0] << ", " <<
                sd.vert()[1] << ", " <<
                sd.vert()[2];

    }

protected:

    // 	static void _bind_methods();

private:

    Vector3 _vert_origin;
    float _total_push_length;

    // mandatory to initialise to have a valid dot
    VectorPtr< Vector3 > _vert;
    VectorPtr< Vector3 > _force;
    
    float _damping;
    uint16_t _kicks;
    
    // normal computation
    Vector3 _normal;
    float _pressure;
    uint16_t _nkicks;

    bool _inititalised;

    void init_internal();

};

#endif // SOFTSKINDOT_H
