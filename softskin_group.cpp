/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skingroup.cpp
 * Author: frankiezafe
 * 
 * Created on May 14, 2019, 7:40 PM
 */

#include "softskin_group.h"

//**************
//* SOFTSKIN GROUP *
//**************

SoftskinGroup::SoftskinGroup() :
_type(sf_UNDEFINED) {
}

const softskin_type_t& SoftskinGroup::get_type() const {
    return _type;
}

String SoftskinGroup::get_name() const {
    return name;
}

void SoftskinGroup::set_name(String s) {
    name = s;
}

void SoftskinGroup::set_std_name(std::string s) {
    name = s.c_str();
}

void SoftskinGroup::_bind_methods() {
    ClassDB::bind_method(D_METHOD("set_name", "name"), &SoftskinGroup::set_name);
    ClassDB::bind_method(D_METHOD("get_name"), &SoftskinGroup::get_name);
}

//******************
//* SOFTSKIN GROUP DOT *
//******************

SoftskinGroupDot::SoftskinGroupDot() {
    _type = sf_DOT;
    _damping = ssk_DEFAULT_DOT_DAMPING;
    _pressure = ssk_DEFAULT_DOT_PESSURE;
}

float SoftskinGroupDot::get_damping() const {
    return _damping;
}

void SoftskinGroupDot::set_damping(const float& f) {
    _damping = f;
    for (uint32_t i = 0; i < _data_num; ++i) {
        _data[i]->set_damping(f * _weights[i]);
    }
}

float SoftskinGroupDot::get_pressure() const {
    return _pressure;
}

void SoftskinGroupDot::set_pressure(const float& f) {
    _pressure = f;
    for (uint32_t i = 0; i < _data_num; ++i) {
        _data[i]->set_pressure(f * _weights[i]);
    }
}

void SoftskinGroupDot::_bind_methods() {

    ClassDB::bind_method(D_METHOD("get_damping"), &SoftskinGroupDot::get_damping);
    ClassDB::bind_method(D_METHOD("set_damping"), &SoftskinGroupDot::set_damping);
    ClassDB::bind_method(D_METHOD("get_pressure"), &SoftskinGroupDot::get_pressure);
    ClassDB::bind_method(D_METHOD("set_pressure"), &SoftskinGroupDot::set_pressure);

}


//*******************
//* SOFTSKIN GROUP EDGE *
//*******************

SoftskinGroupEdge::SoftskinGroupEdge() {
    _type = sf_EDGE;
}

Vector3 SoftskinGroupEdge::get_barycenter() {

    if (_data_num == 0) {
        return Vector3(0, 0, 0);
    }
    Vector3 out;
    int div = 0;
    for (uint32_t i = 0; i < _data_num; ++i) {
        SoftskinEdge* se = _data[i];
        switch (se->get_type()) {
            case sf_FIBER:
            case sf_TENSOR:
                div++;
                out += ((SoftskinFiber*) se)->get_middle();
                break;
            case sf_LIGAMENT:
            case sf_MUSCLE:
                div++;
                out += ((SoftskinLigament*) se)->get_head()->anchor().ref();
                break;
            default:
                break;
        }
    }
    if (div == 0) {
        return Vector3(0, 0, 0);
    }
    return out / div;

}

void SoftskinGroupEdge::_bind_methods() {

    ClassDB::bind_method(D_METHOD("size"), &SoftskinGList<SoftskinEdge>::size);
    ClassDB::bind_method(D_METHOD("empty"), &SoftskinGList<SoftskinEdge>::empty);
    ClassDB::bind_method(D_METHOD("set_stiffness", "stiffness"), &SoftskinGroupEdge::set_stiffness);
    ClassDB::bind_method(D_METHOD("get_stiffness"), &SoftskinGroupEdge::get_stiffness);
    ClassDB::bind_method(D_METHOD("set_shrink", "shrink"), &SoftskinGroupEdge::set_rest_len_multiplier);
    ClassDB::bind_method(D_METHOD("get_shrink"), &SoftskinGroupEdge::get_rest_len_multiplier);
    ClassDB::bind_method(D_METHOD("set_muscle_frequency", "muscle_frequency"), &SoftskinGroupEdge::set_muscle_frequency);
    ClassDB::bind_method(D_METHOD("get_muscle_frequency"), &SoftskinGroupEdge::get_muscle_frequency);
    ClassDB::bind_method(D_METHOD("set_muscle_min", "muscle_min"), &SoftskinGroupEdge::set_muscle_min);
    ClassDB::bind_method(D_METHOD("get_muscle_min"), &SoftskinGroupEdge::get_muscle_min);
    ClassDB::bind_method(D_METHOD("set_muscle_max", "muscle_max"), &SoftskinGroupEdge::set_muscle_max);
    ClassDB::bind_method(D_METHOD("get_muscle_max"), &SoftskinGroupEdge::get_muscle_max);

    ClassDB::bind_method(D_METHOD("get_barycenter"), &SoftskinGroupEdge::get_barycenter);

}

//********************
//* SOFTSKIN GROUP FIBER *
//********************

SoftskinGroupFiber::SoftskinGroupFiber() {
    _type = sf_FIBER;
}

void SoftskinGroupFiber::check_type(bool only_last) {

    if (_list.empty()) return;

    if (only_last) {
        if (_list.size() == 1) {
            _type = _list[0]->get_type();
        } else if (_type == sf_TENSOR && _type != _list[_list.size() - 1]->get_type()) {
            // reverting type to basic one
            _type = sf_FIBER;
        }
        return;
    }

    bool all_tensor = true;
    for (int i = 0, imax = _list.size(); i < imax; ++i) {
        if (_list[i]->get_type() != sf_TENSOR) {
            all_tensor = false;
            break;
        }
    }
    if (all_tensor) {
        _type = sf_TENSOR;
    } else {
        _type = sf_FIBER;
    }
}

Vector3 SoftskinGroupFiber::get_barycenter() {

    if (_data_num == 0) {
        return Vector3(0, 0, 0);
    }
    Vector3 out;
    for (uint32_t i = 0; i < _data_num; ++i) {
        out += _data[i]->get_middle();
    }
    return out / _data_num;

}

void SoftskinGroupFiber::_bind_methods() {

    ClassDB::bind_method(D_METHOD("size"), &SoftskinGList<SoftskinFiber>::size);
    ClassDB::bind_method(D_METHOD("empty"), &SoftskinGList<SoftskinFiber>::empty);
    ClassDB::bind_method(D_METHOD("set_stiffness", "stiffness"), &SoftskinGroupFiber::set_stiffness);
    ClassDB::bind_method(D_METHOD("get_stiffness"), &SoftskinGroupFiber::get_stiffness);
    ClassDB::bind_method(D_METHOD("set_shrink", "shrink"), &SoftskinGroupFiber::set_rest_len_multiplier);
    ClassDB::bind_method(D_METHOD("get_shrink"), &SoftskinGroupFiber::get_rest_len_multiplier);
    ClassDB::bind_method(D_METHOD("set_muscle_frequency", "muscle_frequency"), &SoftskinGroupFiber::set_muscle_frequency);
    ClassDB::bind_method(D_METHOD("get_muscle_frequency"), &SoftskinGroupFiber::get_muscle_frequency);
    ClassDB::bind_method(D_METHOD("set_muscle_min", "muscle_min"), &SoftskinGroupFiber::set_muscle_min);
    ClassDB::bind_method(D_METHOD("get_muscle_min"), &SoftskinGroupFiber::get_muscle_min);
    ClassDB::bind_method(D_METHOD("set_muscle_max", "muscle_max"), &SoftskinGroupFiber::set_muscle_max);
    ClassDB::bind_method(D_METHOD("get_muscle_max"), &SoftskinGroupFiber::get_muscle_max);

    ClassDB::bind_method(D_METHOD("get_barycenter"), &SoftskinGroupFiber::get_barycenter);

}

//***********************
//* SOFTSKIN GROUP LIGAMENT *
//***********************

SoftskinGroupLigament::SoftskinGroupLigament() {
    _type = sf_LIGAMENT;
}

void SoftskinGroupLigament::check_type(bool only_last) {

    if (_list.empty()) return;

    if (only_last) {
        if (_list.size() == 1) {
            _type = _list[0]->get_type();
        } else if (_type == sf_MUSCLE && _type != _list[_list.size() - 1]->get_type()) {
            // reverting type to basic one
            _type = sf_LIGAMENT;
        }
        return;
    }

    bool all_muscle = true;
    for (int i = 0, imax = _list.size(); i < imax; ++i) {
        if (_list[i]->get_type() != sf_MUSCLE) {
            all_muscle = false;
            break;
        }
    }
    if (all_muscle) {
        _type = sf_MUSCLE;
    } else {
        _type = sf_LIGAMENT;
    }
}

Vector3 SoftskinGroupLigament::get_barycenter() {

    if (_data_num == 0) {
        return Vector3(0, 0, 0);
    }
    Vector3 out;
    for (uint32_t i = 0; i < _data_num; ++i) {
        out += _data[i]->get_head()->anchor().ref();
    }
    return out / _data_num;

}

void SoftskinGroupLigament::parent(Node* p) {

    Spatial* s = Object::cast_to<Spatial>(p);
    ERR_FAIL_NULL(s);

    for (uint32_t i = 0; i < _data_num; ++i) {
        _data[i]->parent(s);
    }
}

void SoftskinGroupLigament::_bind_methods() {

    ClassDB::bind_method(D_METHOD("size"), &SoftskinGroupLigament::size);
    ClassDB::bind_method(D_METHOD("empty"), &SoftskinGroupLigament::empty);
    ClassDB::bind_method(D_METHOD("set_stiffness", "stiffness"), &SoftskinGroupLigament::set_stiffness);
    ClassDB::bind_method(D_METHOD("get_stiffness"), &SoftskinGroupLigament::get_stiffness);
    ClassDB::bind_method(D_METHOD("set_shrink", "shrink"), &SoftskinGroupLigament::set_rest_len_multiplier);
    ClassDB::bind_method(D_METHOD("get_shrink"), &SoftskinGroupLigament::get_rest_len_multiplier);
    ClassDB::bind_method(D_METHOD("set_muscle_frequency", "muscle_frequency"), &SoftskinGroupLigament::set_muscle_frequency);
    ClassDB::bind_method(D_METHOD("get_muscle_frequency"), &SoftskinGroupLigament::get_muscle_frequency);
    ClassDB::bind_method(D_METHOD("set_muscle_min", "muscle_min"), &SoftskinGroupLigament::set_muscle_min);
    ClassDB::bind_method(D_METHOD("get_muscle_min"), &SoftskinGroupLigament::get_muscle_min);
    ClassDB::bind_method(D_METHOD("set_muscle_max", "muscle_max"), &SoftskinGroupLigament::set_muscle_max);
    ClassDB::bind_method(D_METHOD("get_muscle_max"), &SoftskinGroupLigament::get_muscle_max);

    ClassDB::bind_method(D_METHOD("get_barycenter"), &SoftskinGroupLigament::get_barycenter);
    ClassDB::bind_method(D_METHOD("parent", "node"), &SoftskinGroupLigament::parent, DEFVAL(0));

}