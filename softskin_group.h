/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_group.h
 * Author: frankiezafe
 *
 * Created on May 14, 2019, 7:40 PM
 */

#ifndef SOFTSKINGROUP_H
#define SOFTSKINGROUP_H

#include "softskin_common.h"
#include "softskin_fiber.h"

class SoftskinGroup : public Object {
    GDCLASS(SoftskinGroup, Object);

public:

    SoftskinGroup();

    //***********
    //* GETTERS *
    //***********

    const softskin_type_t& get_type() const;

    String get_name() const;

    //***********
    //* SETTERS *
    //***********

    void set_name(String s);

    void set_std_name(std::string s);

protected:

    softskin_type_t _type;

    String name;

    static void _bind_methods();

};

template< class T >
class SoftskinGList : public SoftskinGroup {
public:

    SoftskinGList() :
    _packed(false),
    _data_num(0),
    _data(0),
    _weights(0) {
    }

    ~SoftskinGList() {
        if (_data) {
            memdelete_arr(_data);
        }
        if (_weights != 0) {
            memdelete_arr(_weights);
        }
    }

    //**************
    //* PROCESSING *
    //**************

    void push_back(T* element) {
        assert(!_packed);
        if (_type == sf_UNDEFINED) {
            std::cout <<
                    "SoftskinGroup::add, "
                    "type your group before adding anything!" <<
                    std::endl;
            return;
        }

        if (_list.find(element) != -1) {
            std::cout << "SoftskinGroup::add, already in this group!" << std::endl;
            return;
        }
        _list.push_back(element);
        check_type(true);
    }

    void erase(T* element) {
        assert(!_packed);
        if (_type == sf_UNDEFINED) {
            std::cout <<
                    "SoftskinGroup::add, "
                    "type your group before adding anything!" <<
                    std::endl;
            return;
        }
        _list.erase(element);
        check_type();
    }

    /* call this once all the elements are pushed in the group, it will lock
     * the group in edition and enable its usage (all methods of specialised 
     * daughters classes)
     */
    void pack() {
        if (_packed) return;
        _data_num = _list.size();
        if (_data_num > 0) {
            _data = memnew_arr(T*, _data_num);
            _weights = memnew_arr(real_t, _data_num);
            for (uint32_t i = 0; i < _data_num; ++i) {
                _data[i] = _list[i];
                _weights[i] = 1.0;
            }
            // dropping the list
            _list.resize(0);
        }
        _packed = true;
    }

    /* fast and insecure way to load a group without using slow push_back
     * method; it will load the arrays without any check on length and set the
     * _packed flag to true, so verify 2 times or it will exploded at the 
     * first call...
     * 
     */
    void load(uint32_t num, T** data, real_t* weights) {
        assert(!_packed);
        _data_num = num;
        _data = data;
        _weights = weights;
        _packed = true;
    }

    //***********
    //* GETTERS *
    //***********

    _FORCE_INLINE_ const bool& is_packed() const {
        return _packed;
    }

    int size() const {
        if (_packed) return _data_num;
        return _list.size();
    }

    bool empty() const {
        if (_packed) return _data_num == 0;
        return _list.size() == 0;
    }

protected:


    Vector< T* > _list;

    bool _packed;
    uint32_t _data_num;
    T** _data;
    real_t* _weights;

    virtual void check_type(bool only_last = false) {
    }

};

//*******************
//* SPECIALISATIONS *
//*******************

template< class T >
class SoftskinGListEdge : public SoftskinGList< T > {
public:

    SoftskinGListEdge() :
    _stiffness(ssk_DEFAULT_STIFFNESS),
    _rest_len_multiplier(ssk_DEFAULT_REST_LEN_MULT),
    _muscle_frequency(ssk_DEFAULT_MUSCLE_FREQUENCY),
    _muscle_min(ssk_DEFAULT_MUSCLE_MIN_LEN),
    _muscle_max(ssk_DEFAULT_MUSCLE_MAX_LEN) {
    }

    float get_stiffness() const {
        return _stiffness;
    }

    void set_stiffness(float f) {
        _stiffness = f;
        for (uint32_t i = 0; i < SoftskinGList<T>::_data_num; ++i) {
            SoftskinGList<T>::_data[i]->set_stiffness(f * SoftskinGList<T>::_weights[i]);
        }
    }

    float get_rest_len_multiplier() const {
        return _rest_len_multiplier;
    }

    void set_rest_len_multiplier(float f) {
        _rest_len_multiplier = f;
        for (uint32_t i = 0; i < SoftskinGList<T>::_data_num; ++i) {
            SoftskinGList<T>::_data[i]->set_rest_len_multiplier(f * SoftskinGList<T>::_weights[i]);
        }
    }

    float get_muscle_frequency() const {
        return _muscle_frequency;
    }

    void set_muscle_frequency(float f) {
        _muscle_frequency = f;
        for (uint32_t i = 0; i < SoftskinGList<T>::_data_num; ++i) {
            SoftskinGList<T>::_data[i]->set_muscle_frequency(f);
        }
    }

    float get_muscle_min() const {
        return _muscle_min;
    }

    void set_muscle_min(float f) {
        _muscle_min = f;
        for (uint32_t i = 0; i < SoftskinGList<T>::_data_num; ++i) {
            SoftskinGList<T>::_data[i]->set_muscle_min(f);
        }
    }

    float get_muscle_max() const {
        return _muscle_max;
    }

    void set_muscle_max(float f) {
        _muscle_max = f;
        for (uint32_t i = 0; i < SoftskinGList<T>::_data_num; ++i) {
            SoftskinGList<T>::_list[i]->set_muscle_max(f);
        }
    }

    virtual Vector3 get_barycenter() = 0;

protected:

    real_t _stiffness;
    real_t _rest_len_multiplier;
    real_t _muscle_frequency;
    real_t _muscle_min;
    real_t _muscle_max;

};

//*******************
//* SPECIALISATIONS *
//*******************

class SoftskinGroupDot : public SoftskinGList< SoftskinDot > {
    GDCLASS(SoftskinGroupDot, SoftskinGList<SoftskinDot>);

public:

    SoftskinGroupDot();

    float get_damping() const;

    void set_damping(const float& f);

    float get_pressure() const;

    void set_pressure(const float& f);

protected:

    static void _bind_methods();

    real_t _damping;
    real_t _pressure;

};

class SoftskinGroupEdge : public SoftskinGListEdge<SoftskinEdge> {
    GDCLASS(SoftskinGroupEdge, SoftskinGListEdge<SoftskinEdge>);

public:

    SoftskinGroupEdge();

    Vector3 get_barycenter();

protected:

    static void _bind_methods();

};

class SoftskinGroupFiber : public SoftskinGListEdge<SoftskinFiber> {
    GDCLASS(SoftskinGroupFiber, SoftskinGListEdge<SoftskinFiber>);

public:

    SoftskinGroupFiber();

    Vector3 get_barycenter();

protected:

    static void _bind_methods();

    void check_type(bool only_last = false);

};

class SoftskinGroupLigament : public SoftskinGListEdge<SoftskinLigament> {
    GDCLASS(SoftskinGroupLigament, SoftskinGListEdge<SoftskinLigament>);

public:

    SoftskinGroupLigament();

    Vector3 get_barycenter();

    void parent(Node* p);

protected:

    static void _bind_methods();

    void check_type(bool only_last = false);

};

#endif /* SOFTSKINGROUP_H */